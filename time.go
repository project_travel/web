package server

import (
	"encoding/json"
	"strconv"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Time wraps time.Time overriddin the json marshal/unmarshal to pass
// timestamp as integer
type Time struct {
	time.Time `bson:",inline"`
}

var NilTime Time = Time{}

// NewTime create a new Time object with the given format.
func NewTime(t time.Time) *Time {
	return &Time{Time: t.UTC()}
}

func (t *Time) Nil() bool {
	return t.Unix() == NilTime.Unix() || t == nil
}

// MarshalJSON implements json.Marshaler interface.
func (t Time) MarshalJSON() ([]byte, error) {
	if t.Time.IsZero() {
		return []byte("null"), nil
	}
	ret := t.Time.UTC().Format(time.RFC3339)
	return []byte(`"` + ret + `"`), nil
}

// UnmarshalJSON implements json.Unmarshaler inferface.
func (t *Time) UnmarshalJSON(buf []byte) error {
	defer func() {
		if t != nil {
			t.Time = t.Time.UTC()
		}
	}()

	// Try to parse the timestamp integer
	// Without ""
	var tmp string
	if len(buf) >= 2 {
		tmp = string(buf[1 : len(buf)-1])
	} else {
		tmp = string(buf)
	}
	ts, err := strconv.ParseInt(tmp, 10, 64)
	// log.Println(ts, err)
	if err == nil {
		if len(tmp) == 19 {
			t.Time = time.Unix(ts/1e9, ts%1e9)
		} else {
			t.Time = time.Unix(ts, 0)
		}
		return nil
	}
	// Try the default unmarshal
	err = json.Unmarshal(buf, &t.Time)
	if err == nil {
		return nil
	}

	str := strings.Trim(string(buf), `"`)
	if str == "null" || str == "" {
		return nil
	}

	tt, err := time.Parse(time.RubyDate, strings.Trim(string(buf), `"`))
	if err == nil {
		t.Time = tt
		return nil
	}

	tt1, err := time.Parse(time.RFC3339, strings.Trim(string(buf), `"`))
	if err == nil {
		t.Time = tt1
		return nil
	}

	return err
}

// GetBSON implements mgo/bson.Getter interface.
func (t Time) GetBSON() (interface{}, error) {
	if t.Time.IsZero() {
		return nil, nil
	}
	return t.Time, nil
}

// SetBSON implements mgo/bson.Setter interface.
func (t *Time) SetBSON(raw bson.Raw) error {
	defer func() {
		if t != nil {
			t.Time = t.Time.UTC()
		}
	}()
	// Try the default unmarshal
	if err := raw.Unmarshal(&t.Time); err == nil {
		return nil
	}

	// Try to pull the timestamp as an int
	var tsInt int64
	if err := raw.Unmarshal(&tsInt); err == nil {
		if tsInt > 5e9 {
			t.Time = time.Unix(tsInt/1e9, tsInt%1e9)
		} else {
			t.Time = time.Unix(tsInt, 0)
		}
	}

	// Try to pull the timestamp as a string
	var tsStr string
	if err := raw.Unmarshal(&tsStr); err != nil {
		return err
	}
	if tsStr == "" {
		return nil
	}
	ts, err := strconv.ParseInt(tsStr, 10, 64)
	if err == nil {
		if len(tsStr) == 19 {
			t.Time = time.Unix(ts/1e9, ts%1e9)
		} else {
			t.Time = time.Unix(ts, 0)
		}
		return nil
	}

	// Try the json umarshal
	if err := json.Unmarshal([]byte(`"`+tsStr+`"`), &t.Time); err != nil {
		return err
	}
	return nil
}

package server

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

type ReservedWords []string

func (r ReservedWords) In(w string) bool {
	for _, word := range r {
		if word == w {
			return true
		}
	}
	return false
}

var Reserved = ReservedWords{
	"api",
	"wall",
	"test",
	"api-docs",
	"static",
	"service",
	"notifications",
	"notification",
	"me",
}

type UnverifiedUser struct {
	User    *User         `bson:"user"`
	Created *Time         `bson:"created"`
	Id      bson.ObjectId `bson:"_id"`
}

func (s *Server) createUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	user := new(User)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	user.Password = base64.StdEncoding.EncodeToString([]byte(user.Password))

	// Check gender field
	g, err := checkGender(user.Gender)
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "gender can be either male or female"))
	}
	user.Gender = g

	if user.Username == "" || user.Password == "" || user.Email == "" {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "missing nickname, email or password fields"))
	}

	if !isValidEmail(user.Email) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "email must contain @ symbol"))
	}

	if !isValidUsername(user.Username) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "username must start with letter, be less than 20 symbols and contain alphanumerical symbols"))
	}

	if Reserved.In(user.Username) {
		return c.JSON(http.StatusConflict, ErrUsernameReseved.Localize(lang, s.translator))
	}

	if user.Status != "" && !isValidStatus(user.Status) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "status can't be longer than 140 letters"))
	}

	// Check that user doesn't exist
	if err := s.DB.Collections["users"].Get(bson.M{"username": user.Username}, bson.M{"_id": 1}, nil); err == nil {
		return c.JSON(http.StatusBadRequest, ErrUsernameTaken.Localize(lang, s.translator))
	} else {
		if !(isNotFound(err)) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.DB.Collections["users"].Get(bson.M{"email": user.Email}, bson.M{"_id": 1}, nil); err == nil {
		return c.JSON(http.StatusBadRequest, ErrEmailTaken.Localize(lang, s.translator))
	} else {
		if !(isNotFound(err)) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	responseToken := &ResponseToken{}
	if user.AppId != "" && user.Device != "" && user.PushId != "" {
		if err := s.DB.Collections["apps"].Get(bson.M{"secret": user.AppId}, nil, nil); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrInvalidAppId.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}

		}

		refreshToken, err := s.CreateRefreshToken(user.Username, user.AppId, user.Device, user.PushId, true)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}

		if err := s.DB.Collections["tokens"].Create(refreshToken); err == nil {
			responseToken.RefreshToken = refreshToken.Token
		}

	}

	// set registration date
	user.RegistrationDate = NewTime(time.Now().UTC())
	// set role of new user
	user.Role = "user"

	if err := s.DB.Collections["users"].Create(user); err != nil {
		if mgo.IsDup(err) {
			return c.JSON(http.StatusConflict, ErrUsernameTaken.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	token, err := s.CreateAccessToken(user.Username)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	responseToken.AccessToken = token

	// id := bson.NewObjectId()

	// unverifiedUser := &UnverifiedUser{User: user, Created: NewTime(time.Now().UTC()), Id: id}

	// if err := s.DB.Collections["unverifiedUsers"].Create(unverifiedUser); err != nil {
	// 	if mgo.IsDup(err) {
	// 		return c.JSON(http.StatusConflict, Error{ErrInvalidContent, "User already in registration queue"})
	// 	} else {
	// 		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// 	}
	// }

	// go func() {
	// 	lang := getLanguage(c, s.translator)
	// 	if err := s.MailClient.SendVerification(user.Email, user.Username, id.Hex(), lang); err != nil {
	// 		s.logger.WithError(err).Error("Can't send mail")
	// 	}
	// }()

	return c.JSON(http.StatusCreated, responseToken)
}

func (s *Server) deleteUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	if err := s.DB.Collections["tokens"].DeleteAll(RefreshToken{Username: username}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.DB.Collections["users"].Delete(bson.M{"username": username}); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.DB.Collections["posts"].DeleteAll(bson.M{"owner": username}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// User is the only owner of the trip -> remove it
	if err := s.DB.Collections["trips"].DeleteAll(bson.M{
		"$and": []bson.M{
			bson.M{"owners": username},
			bson.M{"owners": bson.M{"$size": 1}},
		},
	}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// User isn't the only owner of the trip -> remove him from "owners" array
	if err := s.DB.Collections["trips"].UpdateAll(
		bson.M{
			"$where": "this.owners.length != 1 && " + username + " in this.owners",
		},
		bson.M{
			"$pull": bson.M{"owners": username},
		}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.DB.Collections["notifications"].DeleteAll(bson.M{"owner": username}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) changePassword(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	req := new(ChangePassword)
	if err := c.Bind(req); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}
	if req.OldPassword == "" || req.NewPassword == "" {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "password can't be an empty string"))
	}
	if req.OldPassword == req.NewPassword {
		return c.JSON(http.StatusBadRequest, ErrOldAndNewSamePasswords.Localize(lang, s.translator))
	}
	req.OldPassword = base64.StdEncoding.EncodeToString([]byte(req.OldPassword))
	req.NewPassword = base64.StdEncoding.EncodeToString([]byte(req.NewPassword))

	search := bson.M{"username": username, "password": req.OldPassword}
	update := bson.M{"$set": bson.M{"password": req.NewPassword}}
	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "wrong password"))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	return c.JSON(http.StatusNoContent, nil)

}

func (s *Server) updateUser(c echo.Context) error {
	username := getUsernameFromToken(c)
	lang := getLanguage(c, s.translator)

	user := new(UpdateUser)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	// Check gender field
	if user.Gender != nil {
		tmp, err := checkGender(*user.Gender)
		if err != nil {
			return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "gender can be either male or female"))
		}
		user.Gender = &tmp
	}

	search := bson.M{"username": username}
	update := bson.M{"$set": user}

	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	return c.JSON(http.StatusNoContent, nil)

}

func (s *Server) getUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username, _ := getUsername(c)
	currentUsername := getUsernameFromToken(c)

	search := bson.M{"username": username}

	user := new(User)

	if err := s.DB.Collections["users"].Aggregate([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(currentUsername),
		},
	}, user); err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, user)
}

func (s *Server) getUsers(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	offset, size, order, err := setPagination(c, "username")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search := formUserSearchQuery(c.QueryParam("q"))

	users := make([]*User, 0)

	if err := s.DB.Collections["users"].AggregateAll([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(getUsernameFromToken(c)),
		},
		bson.M{
			"$sort": formSortQuery(order),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &users); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// // Don't show password field
	// for i := 0; i < len(users); i++ {
	// 	users[i].Password = ""
	// }

	return c.JSON(http.StatusOK, users)

}

func (s *Server) getFollowers(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username, _ := getUsername(c)
	search := bson.M{"username": username}

	user := new(User)
	if err := s.DB.Collections["users"].Get(search, bson.M{"followers": 1}, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if len(user.Followers) == 0 {
		return c.JSON(http.StatusOK, []string{})
	}

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search = formUserSearchQuery(c.QueryParam("q"))
	search["username"] = bson.M{"$in": user.Followers}

	// search = bson.M{"username": bson.M{"$in": user.Followers}}
	users := make([]*User, 0)

	if err := s.DB.Collections["users"].AggregateAll([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(getUsernameFromToken(c)),
		},
		bson.M{
			"$sort": formSortQuery("lastname,firstname,username"),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &users); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}
	return c.JSON(http.StatusOK, users)
}

func (s *Server) getFollowing(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username, _ := getUsername(c)
	search := bson.M{"username": username}

	user := new(User)
	if err := s.DB.Collections["users"].Get(search, bson.M{"following": 1}, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if len(user.Following) == 0 {
		return c.JSON(http.StatusOK, []string{})
	}

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	users := make([]*User, 0)
	search = formUserSearchQuery(c.QueryParam("q"))
	search["username"] = bson.M{"$in": user.Following}
	// search = bson.M{"username": bson.M{"$in": user.Following}}
	if err := s.DB.Collections["users"].AggregateAll([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(getUsernameFromToken(c)),
		},
		bson.M{
			"$sort": formSortQuery("lastname,firstname,username"),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &users); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}
	return c.JSON(http.StatusOK, users)
}

func (s *Server) followUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	friend := c.Param("stranger")

	if username == friend || friend == "me" {
		return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
	}

	s.logger.WithFields(logrus.Fields{
		"username":     username,
		"newFollowing": friend,
	}).Debug("New following")

	// Check that user isn't in blacklist
	if err := s.DB.Collections["users"].Get(bson.M{"username": username, "blacklist": bson.M{"$ne": friend}}, bson.M{"_id": 1}, nil); err != nil {
		return s.handleUserError(c, err, username, ErrFollowingBlacklist.Localize(lang, s.translator))
	}

	// Update friend collection
	search := bson.M{"username": friend, "followers": bson.M{"$ne": username}}
	update := bson.M{"$push": bson.M{"followers": username}, "$inc": bson.M{"lengthFollowers": 1}}

	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return s.handleUserError(c, err, friend, ErrFollowing.Localize(lang, s.translator))
	}

	search = bson.M{"username": username, "following": bson.M{"$ne": friend}}
	update = bson.M{"$push": bson.M{"following": friend}, "$inc": bson.M{"lengthFollowing": 1}}

	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return s.handleUserError(c, err, friend, ErrFollowing.Localize(lang, s.translator))
	}

	// Send push notifications
	go func() {
		if err := s.Pusher.Send(friend, "follower", username); err != nil {
			log.Println("Can't send push ->", err)
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) unfollowUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	enemy := c.Param("stranger")

	s.logger.WithFields(logrus.Fields{
		"username":     username,
		"oldFollowing": enemy,
	}).Debug("Old following")

	// If user exists, remove from his followers list
	search := bson.M{"username": enemy, "followers": username}
	update := bson.M{"$pull": bson.M{"followers": username}, "$inc": bson.M{"lengthFollowers": -1}}
	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return s.handleUserError(c, err, enemy, ErrNotFollowing.Localize(lang, s.translator))
	}

	search = bson.M{"username": username, "following": enemy}
	update = bson.M{"$pull": bson.M{"following": enemy}, "$inc": bson.M{"lengthFollowing": -1}}
	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return s.handleUserError(c, err, enemy, ErrNotFollowing.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) getPossibleFriends(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	search := bson.M{"username": username}

	user := new(User)
	if err := s.DB.Collections["users"].Get(search, bson.M{"possibleFollowing": 1}, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if len(user.PossibleFollowing) == 0 {
		return c.JSON(http.StatusOK, []string{})
	}

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search["username"] = bson.M{"$in": user.PossibleFollowing}

	users := make([]*User, 0)

	project := []bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(username),
		},
	}
	if c.QueryParam("random") == "true" {
		// shuffle
		project = append(project, bson.M{
			"$sample": bson.M{"size": size},
		})
	} else {
		// add pagination as usual
		project = append(project, []bson.M{
			bson.M{
				"$sort": formSortQuery("lastname,firstname,username"),
			},
			bson.M{
				"$skip": offset,
			},
			bson.M{
				"$limit": size,
			},
		}...)
	}

	if err := s.DB.Collections["users"].AggregateAll(project, &users); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	return c.JSON(http.StatusOK, users)
}

func (s *Server) getLocale(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	return c.JSON(http.StatusOK, map[string]string{"language": lang})
}

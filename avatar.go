package server

import (
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"path"
	"strings"
	"sync"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type ImageUpload struct {
	Data []byte
	Name string
	Size []uint
	Type string
}

var AvatarSizes = [][]uint{[]uint{150, 150}, []uint{80, 80}, []uint{300, 300}}
var BackgroundSizes = [][]uint{[]uint{750, 400}, []uint{1125, 600}}

// toImagesUpload creates set of images upload requests
func toImagesUpload(f *multipart.FileHeader, sizes [][]uint) ([]ImageUpload, error) {
	// check if we can work with data
	if err := isResizable(f.Filename); err != nil {
		return nil, err
	}
	// in different sizes + original one
	res := make([]ImageUpload, len(sizes)+1)

	file, err := f.Open()
	if err != nil {
		return nil, err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	// get image size
	size, err := imageSize(data, path.Ext(f.Filename))
	if err != nil {
		return nil, err
	}

	// The last image is the original one
	res[len(res)-1] = ImageUpload{Data: data, Name: ".png", Size: size, Type: "original"}

	var wg sync.WaitGroup
	wg.Add(len(sizes))
	var globalErr error

	for i := 0; i < len(res)-1; i++ {
		go func(i int) {
			defer wg.Done()
			// tmp := make([]byte, 0)
			// newSize := make([]uint, 0)
			// Resize image
			tmp, newSize, err := Resize(data, path.Ext(f.Filename), ".png", sizes[i][0], sizes[i][1], true)
			if err != nil {
				globalErr = err
				return
			}

			// Label thumb images
			if sizes[i][0] == 150 && sizes[i][1] == 150 {
				res[i] = ImageUpload{Data: tmp, Name: ".png", Size: newSize, Type: "thumb"}
			} else {
				res[i] = ImageUpload{Data: tmp, Name: ".png", Size: newSize}
			}

		}(i)
	}
	wg.Wait()
	if globalErr != nil {
		return nil, globalErr
	}
	return res, nil
}

func (s *Server) sendImages(images []ImageUpload) ([]*Photo, error) {
	s.logger.Debug("Sending images")

	photos := make([]*Photo, len(images))

	var wg sync.WaitGroup
	wg.Add(len(images))

	var globalErr error

	for i, im := range images {
		go func(i int, im ImageUpload) {
			defer wg.Done()
			// var url *StorageResponse
			url, err := s.Storage.Send(im)
			if err != nil {
				globalErr = err
				return
			}
			photos[i] = &Photo{Width: int(im.Size[0]), Height: int(im.Size[1]), Url: url.Url}
			if im.Type != "" {
				photos[i].Type = im.Type
			}
		}(i, im)
	}
	wg.Wait()
	if globalErr != nil {
		s.logger.WithField("error", globalErr).Error("Can't send upload images request")
		return nil, globalErr
	}

	for i, photo := range photos {
		if strings.Contains(photo.Url, s.Storage.Address) {
			continue
		}

		// Add host if storage returns absolute path
		u, err := url.Parse(s.Storage.Address)
		if err != nil {
			return nil, err
		}
		u.Path = path.Join(u.Path, photo.Url)
		photos[i].Url = u.String()
	}

	return photos, nil
}

func (s *Server) uploadUserAvatar(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	file, err := c.FormFile("file")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	images, err := toImagesUpload(file, AvatarSizes)
	if err != nil {
		s.logger.WithField("err", err).Error("Can't form upload images request")
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	photos, err := s.sendImages(images)
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	username := getUsernameFromToken(c)
	search := bson.M{"username": username}
	update := bson.M{"$set": bson.M{"avatar": photos}}

	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	return c.JSON(http.StatusCreated, photos)
}

func (s *Server) deleteUserAvatar(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	res := new(User)

	if err := s.DB.Collections["users"].FindAndModify(
		bson.M{"username": username},
		bson.M{"avatar": 1},
		bson.M{"$unset": bson.M{"avatar": 1}},
		false,
		res,
	); err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	// Delete pictures from storage
	for _, avatar := range res.Avatar {
		go s.Storage.Delete(avatar.Url)
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) deleteUserBackground(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	res := new(User)

	if err := s.DB.Collections["users"].FindAndModify(
		bson.M{"username": username},
		bson.M{"background": 1},
		bson.M{"$unset": bson.M{"background": 1}},
		false,
		res,
	); err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	// Delete pictures from storage
	for _, avatar := range res.Background {
		go s.Storage.Delete(avatar.Url)
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) uploadUserBackground(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	file, err := c.FormFile("file")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	images, err := toImagesUpload(file, BackgroundSizes)
	if err != nil {
		s.logger.WithField("err", err).Error("Can't form upload images request")
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	photos, err := s.sendImages(images)
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	username := getUsernameFromToken(c)
	search := bson.M{"username": username}
	update := bson.M{"$set": bson.M{"background": photos}}

	if err := s.DB.Collections["users"].Update(search, update); err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	return c.JSON(http.StatusCreated, photos)
}

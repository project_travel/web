package server

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"strconv"

	"github.com/labstack/echo"

	"bytes"
	"encoding/base64"
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/garyburd/go-oauth/oauth"
	"gopkg.in/mgo.v2/bson"
)

type CredData struct {
	Type     string
	Username string
	Token    string
	Secret   string
}

type TwitterClient struct {
	*oauth.Client
}

func (tw *TwitterConfig) InitClient() {
	tw.Client = &TwitterClient{&oauth.Client{
		TemporaryCredentialRequestURI: "https://api.twitter.com/oauth/request_token",
		ResourceOwnerAuthorizationURI: "https://api.twitter.com/oauth/authorize",
		TokenRequestURI:               "https://api.twitter.com/oauth/access_token",
		Credentials:                   oauth.Credentials{Token: tw.Key, Secret: tw.Secret},
	},
	}
}

type TwitterConfig struct {
	Key         string `json:"key"`
	Secret      string `json:"secret"`
	RedirectURL string `json:"redirectUrl"`
	Client      *TwitterClient
}

type TwitterUser struct {
	Id             string `json:"id_str" bson:"id"`
	Username       string `json:"screen_name" bson:"username"`
	FullName       string `json:"name" bson:"fullName"`
	ProfilePicture string `json:"profile_image_url_https" bson:"profilePicture"`
}

type Tweet struct {
	Id            string         `json:"id_str"`
	Text          string         `json:"text"`
	Entities      *TweetEntities `json:"entities"`
	IsStatusReply string         `json:"in_reply_to_status_id_str"` // id of original tweet
	Time          *Time          `json:"created_at"`
	Likes         int            `json:"favorite_count"`
	Retweet       bool           `json:"retweeted"`
	Geo           *Geo           `json:"coordinates"`
	Place         *TweetPlace    `json:"place"`
	Url           string         `json:"-"` // forms on our side
	// https://twitter.com/USERNAME/status/ID <- form url
}

type TweetEntities struct {
	Tags  []TweetTags   `json:"hashtags"`
	Media []*TweetMedia `json:"media,omitempty"`
}

type TweetTags struct {
	Text string `json:"text"`
}

type TweetMedia struct {
	Url   string                     `json:"media_url"`
	Type  string                     `json:"type"`
	Sizes map[string]TweetMediaSizes `json:"sizes"`
}

type TweetMediaSizes struct {
	Height int `json:"h"`
	Width  int `json:"w"`
}

type TweetPlace struct {
	Name        string `json:"name"`
	CountryCode string `json:"country_code"`
	Country     string `json:"country"`
	Type        string `json:"place_type"`
	Geo         *Geo   `json:"bounding_box"`
}

type TweetFriends struct {
	Next    string   `json:"next_cursor_str"`
	Friends []string `json:"ids"`
}

type CustomRedirect struct {
	Url string `json:"url"`
}

func (s *Server) authorizeTwitter(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	// What user sends request
	userToken := c.Get("access_token").(*jwt.Token)
	claims := userToken.Claims.(jwt.MapClaims)
	username := claims["username"].(string)

	// Get redirect url
	ru := c.QueryParam("redirect_url")
	if ru == "" {
		ru = "/"
	}

	// Pass him to auth twitter URL
	u, err := url.Parse(s.TwitterConfig.RedirectURL)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	q := u.Query()
	q.Set("state", base64.URLEncoding.EncodeToString([]byte(ru)))
	u.RawQuery = q.Encode()
	callback := u.String()
	tempCred, err := s.TwitterConfig.Client.RequestTemporaryCredentials(nil, callback, nil)
	if err != nil {
		log.Println("Error ->", err)
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	// TODO: store credentiels somewhere, redis mb?
	if err := s.DB.Collections["creds"].Create(CredData{
		Type:     "twitter",
		Username: username,
		Token:    tempCred.Token,
		Secret:   tempCred.Secret,
	}); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	if c.QueryParam("no_redirect") == "true" {
		return c.JSON(http.StatusOK, CustomRedirect{s.TwitterConfig.Client.AuthorizationURL(tempCred, nil)})
	} else {
		return c.Redirect(http.StatusFound, s.TwitterConfig.Client.AuthorizationURL(tempCred, nil))
	}

}

// callback handles callbacks from the OAuth server.
func (s *Server) callbackTwitter(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	data := new(CredData)
	if err := s.DB.Collections["creds"].Get(
		bson.M{
			"type":  "twitter",
			"token": c.FormValue("oauth_token"),
		},
		nil,
		data); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Get redirect url
	ru := c.QueryParam("state")

	tempCred := &oauth.Credentials{Token: data.Token, Secret: data.Secret}

	tokenCred, _, err := s.TwitterConfig.Client.RequestToken(nil, tempCred, c.FormValue("oauth_verifier"))
	log.Println("TokenCred ->", tokenCred)
	if err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, err.Error()}, ru)
	}
	if err := s.DB.Collections["creds"].Delete(*data); err != nil {
		log.Println("Error ->", err)
	}

	// Save new token
	search := bson.M{"username": data.Username}
	query := bson.M{"$set": bson.M{
		"twitter": bson.M{
			"accessToken":  tokenCred.Token,
			"accessSecret": tokenCred.Secret,
		},
	},
	}
	if err := s.DB.Collections["users"].Update(search, query); err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusInternalServerError, err.Error()}, ru)
	}

	// Get user information
	user, err := s.getUserInfoTwitter(data.Username)
	if err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, err.Error()}, ru)
	}
	// Save it in db
	query = bson.M{"$set": bson.M{"twitter.user": user}}
	if err := s.DB.Collections["users"].Update(search, query); err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusInternalServerError, err.Error()}, ru)
	}

	go s.addPossibleFriends(data.Username)

	go func() {
		if err := s.saveTweets(data.Username); err != nil {
			s.logger.WithError(err).Debug("Can't save tweets")
		}
		request, err := s.formTripRequest(data.Username, true)
		if err != nil {
			s.logger.WithError(err).Warn("Can't form trip request")
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			s.logger.WithError(err).Error("Can't send rabbit request")
			return
		}
	}()

	return redirectAfterAuth(c, nil, ru)
}

func (s *Server) getUserInfoTwitter(username string) (*TwitterUser, error) {
	// Get user token from db
	token, err := s.getSocialMediaInfo(username, "twitter")
	if err != nil {
		return nil, err
	}

	cred := &oauth.Credentials{Token: token.Token, Secret: token.Secret}
	v := url.Values{}
	v.Set("include_entities", "true")
	v.Set("skip_status", "true")
	v.Set("include_mail", "false")
	response, err := s.TwitterConfig.Client.Get(nil, cred, "https://api.twitter.com/1.1/account/verify_credentials.json", v)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	buf := bytes.Buffer{}
	if _, err := buf.ReadFrom(response.Body); err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, errors.New(buf.String())
	}

	user := new(TwitterUser)
	if err := json.Unmarshal(buf.Bytes(), user); err != nil {
		return nil, err
	}

	return user, nil
}

func (s *Server) saveTweets(username string) error {
	// Get user info from db
	info, err := s.getSocialMediaInfo(username, "twitter")
	if err != nil {
		return err
	}

	if err := s.TaskModule.StartTask(username, "twitter"); err != nil {
		return err
	}

	var maxId, sinceId string

	// Twitter user
	user := info.User
	// Create credentiels for request
	cred := &oauth.Credentials{Token: info.Token, Secret: info.Secret}
	// Read data comming from twitter
	for {
		v := url.Values{}

		if maxId != "" {
			v.Set("max_id", maxId)
		}
		if sinceId != "" {
			v.Set("since_id", sinceId)
		}
		v.Set("user_id", user.Id)
		v.Set("trim_user", "true")
		v.Set("exclude_replies", "false")
		v.Set("count", "200")
		v.Set("lang", "en") // Get response in english
		log.Println("URL values ->", v)

		response, err := s.TwitterConfig.Client.Get(nil, cred, "https://api.twitter.com/1.1/statuses/user_timeline.json", v)
		if err != nil {
			return err
		}
		defer response.Body.Close()
		buf := bytes.Buffer{}
		if _, err := buf.ReadFrom(response.Body); err != nil {
			return err
		}
		if response.StatusCode != http.StatusOK {
			return errors.New(buf.String())
		}

		tweets := make([]Tweet, 0)
		if err := json.Unmarshal(buf.Bytes(), &tweets); err != nil {
			return err
		}
		// Read all tweets
		if len(tweets) == 0 {
			break
		}

		// Find tweet with smallest id. It will be max_id parametr for next iteration
		minIdStr, _ := strconv.ParseInt(tweets[0].Id, 10, 64)
		for _, v := range tweets {
			tmp, _ := strconv.ParseInt(v.Id, 10, 64)
			if tmp < minIdStr {
				minIdStr = tmp
			}

			// Tweet is a reply
			// Store only original tweets
			if v.IsStatusReply != "" {
				continue
			}

			// Set twitter url
			v.Url = fmt.Sprintf("https://twitter.com/%s/status/%s", info.User.Username, v.Id)
			posts := toPosts(v, s.translator)

			if posts != nil {
				// Add owner field
				for i := 0; i < len(posts); i++ {
					posts[i].Owner = username
				}

				for _, post := range posts {
					if err := s.DB.Collections["posts"].Update(
						bson.M{"source.id": post.Source.Id},
						bson.M{"$set": post},
						true,
					); err != nil {
						s.logger.WithError(err).Error("Can't save tweets")
					}
				}
			}

		}

		// It will return self otherwise
		maxId = strconv.FormatInt(minIdStr-1, 10)
	}
	return nil
}

func (s *Server) getTwitterFriends(username string) ([]string, error) {
	// Get user info from db
	info, err := s.getSocialMediaInfo(username, "twitter")
	if err != nil {
		return nil, err
	}
	log.Println("Info ->", info)

	// Twitter user
	user := info.User
	// Create credentiels for request
	cred := &oauth.Credentials{Token: info.Token, Secret: info.Secret}
	buf := bufPool.Get().(*bytes.Buffer)
	buf.Reset()
	defer bufPool.Put(buf)
	next := "-1"

	v := url.Values{}

	v.Set("stringify_ids", "true")
	v.Set("user_id", user.Id)
	v.Set("count", "5000")
	friendsIds := make([]string, 0)

	for {
		v.Set("cursor", next)
		fmt.Println("Next ->", v)
		response, err := s.TwitterConfig.Client.Get(nil, cred, "https://api.twitter.com/1.1/friends/ids.json", v)
		if err != nil {
			return nil, err
		}
		defer response.Body.Close()

		buf.Reset()
		if _, err := buf.ReadFrom(response.Body); err != nil {
			return nil, err
		}

		friends := new(TweetFriends)
		if err := json.Unmarshal(buf.Bytes(), friends); err != nil {
			return nil, err
		}
		friendsIds = append(friendsIds, friends.Friends...)

		if friends.Next == "0" {
			break
		} else {
			next = friends.Next
		}
	}

	return friendsIds, nil

}

func (s *Server) deleteTwitter(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	// Delete credentiels
	if err := s.DB.Collections["users"].Update(
		bson.M{"username": username},
		bson.M{
			"$unset": bson.M{
				"twitter": 1,
			},
		},
	); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.removeSocialMediaData(username, "twitter"); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// // Label posts as history
	// if err := s.DB.Collections["posts"].Update(
	// 	bson.M{
	// 		"owner":       username,
	// 		"source.name": "twitter",
	// 	}, bson.M{
	// 		"$set": bson.M{"history": true},
	// 	},
	// ); err != nil {
	// 	if !isNotFound(err) {
	// 		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// 	}
	// }
	// if err := s.DB.Collections["trips"].Update(
	// 	bson.M{
	// 		"owner":       username,
	// 		"source.name": "twitter",
	// 	}, bson.M{
	// 		"$set":   bson.M{"history": true},
	// 		"$unset": bson.M{"tripId": 1},
	// 	},
	// ); err != nil {
	// 	if !isNotFound(err) {
	// 		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// 	}
	// }
	// Update trips
	go func() {
		request, err := s.formTripRequest(username, true)
		if err != nil {
			log.Println("Can't form request error ->", err)
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			log.Println("Can't send rabbit request ->", err)
			return
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type InstagramConfig struct {
	Id          string `json:"id"`
	Secret      string `json:"secret"`
	RedirectURL string `json:"redirectUrl"`
}

type InstagramUser struct {
	Id             string `json:"id" bson:"id"`
	Username       string `json:"username" bson:"username"`
	FullName       string `json:"full_name" bson:"fullName"`
	ProfilePicture string `json:"profile_picture" bson:"profilePicture"`
}

type InstagramUserData struct {
	Token string        `json:"access_token" bson:"accessToken"`
	User  InstagramUser `json:"user" bson:"user"`
}

type InstagramMediaResponse struct {
	Pagination *InstagramPagination `json:"pagination,omitempty"`
	Meta       struct {
		Meta int `json:"code"`
	} `json:"meta"`
	Data []InstagramData `json:"data"`
}

type InstagramPagination struct {
	NextUrl   string `json:"next_url"`
	NextMaxId string `json:"next_max_id"`
}

type InstagramData struct {
	Id       string            `json:"id"`
	Hashtags []string          `json:"tags"`
	Type     string            `json:"type"`
	Link     string            `json:"link"`
	Created  *Time             `json:"created_time"`
	Photos   map[string]*Photo `json:"images"`
	Text     *struct {
		Text string `json:"text"`
	} `json:"caption"`
	Location *struct {
		Longitude float64 `json:"longitude"`
		Latitude  float64 `json:"latitude"`
		Name      string  `json:"name"`
	} `json:"location'`
	Likes struct {
		Count int `json:"count"`
	} `json:"likes"`
}

type InstagramResponse struct {
	Pagination *InstagramPagination `json:"pagination,omitempty"`
	Meta       struct {
		Meta int `json:"code"`
	} `json:"meta"`
	Data interface{} `json:"data"`
}

// Redirect user to instagram site to auth
func (s *Server) authorizeInstagram(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	// What user sends request
	userToken := c.Get("access_token").(*jwt.Token)
	claims := userToken.Claims.(jwt.MapClaims)
	username := claims["username"].(string)

	// Get redirect url
	ru := c.QueryParam("redirect_url")
	if ru == "" {
		ru = "/"
	}

	// Pass him to auth instagram URL
	u, err := url.Parse("https://api.instagram.com/oauth/authorize/")
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	q := u.Query()
	q.Set("client_id", s.InstagramConfig.Id)
	q.Set("response_type", "code")
	q.Set("redirect_uri", s.InstagramConfig.RedirectURL)
	// Need to know user followers and etc
	q.Set("scope", "follower_list")
	// TODO: check if it works without this step
	q.Set("state", fmt.Sprintf("%s,%s",
		username, base64.URLEncoding.EncodeToString([]byte(ru))))
	u.RawQuery = q.Encode()
	// Right redirect URL in u
	if c.QueryParam("no_redirect") == "true" {
		return c.JSON(http.StatusOK, CustomRedirect{u.String()})
	} else {
		return c.Redirect(http.StatusFound, u.String())
	}
}

func (s *Server) callbackInstagram(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	u := c.Request().URL
	// u, err := url.Parse(c.Request().URI())
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// }
	log.Println("URL from instagram ->", u)
	q := u.Query()
	code := q.Get("code")

	// Get state parametres from URL.
	tmp := strings.Split(q.Get("state"), ",")
	if len(tmp) != 2 {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	username, ru := tmp[0], tmp[1]
	log.Println("Url ->", ru)
	rUrl, err := base64.URLEncoding.DecodeString(ru)
	var redirectUrl string
	if err != nil || len(rUrl) == 0 {
		redirectUrl = "/"
	} else {
		redirectUrl = string(rUrl)
	}
	log.Println("Parsed Url ->", redirectUrl)
	// There's no code in URL if user denied to provide access token
	if code == "" {
		msg := fmt.Sprintf("%s: %s",
			q.Get("error_reason"), q.Get("error_description"))
		log.Println("Msg ->", msg)
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, msg}, ru)
	}
	// Exchange code for access token
	token, err := s.getInstagramAccessToken(username, code)
	if err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, err.Error()}, ru)
	}
	search := bson.M{"username": username}
	query := bson.M{"$set": bson.M{"instagram": token}}
	if err := s.DB.Collections["users"].Update(search, query); err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusInternalServerError, err.Error()}, ru)
	}

	go s.addPossibleFriends(username)

	// Start harvesting data
	go func() {
		if err := s.loadInstagramData(username); err != nil {
			log.Println("Load instagram data ->", err)
		}
		request, err := s.formTripRequest(username, true)
		if err != nil {
			log.Println("Can't form request error ->", err)
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			log.Println("Can't send rabbit request ->", err)
			return
		}
	}()
	return redirectAfterAuth(c, nil, ru)
}

func (s *Server) getInstagramAccessToken(username, code string) (*InstagramUserData, error) {
	// Instagram examle:
	// curl -F 'client_id=CLIENT_ID' \
	//    -F 'client_secret=CLIENT_SECRET' \
	//    -F 'grant_type=authorization_code' \
	//    -F 'redirect_uri=AUTHORIZATION_REDIRECT_URI' \
	//    -F 'code=CODE' \
	//    https://api.instagram.com/oauth/access_token
	v := url.Values{}
	v.Set("client_id", s.InstagramConfig.Id)
	v.Set("grant_type", "authorization_code")
	v.Set("redirect_uri", s.InstagramConfig.RedirectURL)
	v.Set("client_secret", s.InstagramConfig.Secret)
	v.Set("code", code)
	response, err := http.PostForm("https://api.instagram.com/oauth/access_token", v)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	buf := bytes.Buffer{}
	if _, err := buf.ReadFrom(response.Body); err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		codeErr := new(CodeError)
		if err := json.Unmarshal(buf.Bytes(), codeErr); err != nil {
			return nil, err
		}
		return nil, errors.New(codeErr.Error.Message)
	}
	log.Println("User ->", buf.String(), response.StatusCode)
	user := new(InstagramUserData)
	if err := json.Unmarshal(buf.Bytes(), user); err != nil {
		return nil, err
	}
	log.Println("Marshal User ->", user)
	return user, nil
}

func (s *Server) loadInstagramData(username string) error {
	// Get user info from db
	info, err := s.getSocialMediaInfo(username, "instagram")
	if err != nil {
		return err
	}

	if err := s.TaskModule.StartTask(username, "instagram"); err != nil {
		return err
	}

	// Initial value
	u, err := url.Parse("https://api.instagram.com/v1/users/self/media/recent/")
	if err != nil {
		return err
	}
	v := u.Query()

	v.Set("count", "20")

	v.Set("access_token", info.Token)
	u.RawQuery = v.Encode()

	urlValue := u.String()

	// Read data comming from instagram
	for {
		response, err := http.Get(urlValue)
		if err != nil {
			return err
		}
		defer response.Body.Close()
		buf := bytes.Buffer{}
		if _, err := buf.ReadFrom(response.Body); err != nil {
			return err
		}

		// No need to check http status, s instagram always returns 200
		data := new(InstagramMediaResponse)
		if err := json.Unmarshal(buf.Bytes(), data); err != nil {
			return err
		}

		// Read all data
		if len(data.Data) == 0 {
			break
		}

		posts := toPosts(data.Data, s.translator)
		if len(posts) != 0 {
			// Add owner field
			for i := 0; i < len(posts); i++ {
				posts[i].Owner = username
				posts[i].History = false
			}

			for _, post := range posts {
				if err := s.DB.Collections["posts"].Update(
					bson.M{"source.id": post.Source.Id},
					bson.M{"$set": post},
					true,
				); err != nil {
					log.Println("Error saving posts ->", err)
				}
			}
		}
		if data.Pagination != nil {
			urlValue = data.Pagination.NextUrl
			if urlValue == "" {
				break
			}
		} else {
			break
		}

	}
	return nil
}

func (s *Server) getInstagramFriends(username string) ([]string, error) {
	// Get user info from db
	info, err := s.getSocialMediaInfo(username, "instagram")
	if err != nil {
		return nil, err
	}
	log.Println("Info ->", info)

	// Initial value
	u, err := url.Parse("https://api.instagram.com/v1/users/self/follows")
	if err != nil {
		return nil, err
	}
	v := u.Query()

	v.Set("count", "20")

	v.Set("access_token", info.Token)
	u.RawQuery = v.Encode()

	urlValue := u.String()

	friendsIds := make([]string, 0)

	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)

	for {
		response, err := http.Get(urlValue)
		if err != nil {
			return nil, err
		}
		defer response.Body.Close()

		buf.Reset()

		if _, err := buf.ReadFrom(response.Body); err != nil {
			return nil, err
		}

		fmt.Println(buf.String())

		// No need to check http status, s instagram always returns 200
		data := new(InstagramResponse)
		if err := json.Unmarshal(buf.Bytes(), &data); err != nil {
			return nil, err
		}

		friends, ok := data.Data.([]interface{})
		if !ok {
			return nil, errors.New("Can't read response")
		}

		fmt.Println(friends)

		tmp := make([]string, len(friends))
		for i, v := range friends {
			tmp[i] = v.(map[string]interface{})["id"].(string)
		}

		friendsIds = append(friendsIds, tmp...)

		if data.Pagination != nil {
			urlValue = data.Pagination.NextUrl
			if urlValue == "" {
				break
			}
		} else {
			break
		}

	}

	return friendsIds, nil
}

func (s *Server) deleteInstagram(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	// Delete credentiels
	if err := s.DB.Collections["users"].Update(
		bson.M{"username": username},
		bson.M{
			"$unset": bson.M{
				"instagram": 1,
			},
		},
	); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	if err := s.removeSocialMediaData(username, "instagram"); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// // Label posts as history
	// if err := s.DB.Collections["posts"].Update(
	// 	bson.M{
	// 		"owner":       username,
	// 		"source.name": "instagram",
	// 	}, bson.M{
	// 		"$set":   bson.M{"history": true},
	// 		"$unset": bson.M{"tripId": 1},
	// 	},
	// ); err != nil {
	// 	if !isNotFound(err) {
	// 		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// 	}
	// }
	// if err := s.DB.Collections["trips"].Update(
	// 	bson.M{
	// 		"owner":       username,
	// 		"source.name": "instagram",
	// 	}, bson.M{
	// 		"$set": bson.M{"history": true},
	// 	},
	// ); err != nil {
	// 	if !isNotFound(err) {
	// 		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// 	}
	// }
	// Update trips
	go func() {
		request, err := s.formTripRequest(username, true)
		if err != nil {
			log.Println("Can't form request error ->", err)
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			log.Println("Can't send rabbit request ->", err)
			return
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

all: def

def:
	cd main && go build -o web && cd ..

docker: Dockerfile
	cd main && env GOOS=linux GOARCH=amd64 go build -o web_docker && cd ..
	@if [ "$(docker ps -a | grep registry.gitlab.com/inspiry/api)" != "" ]; then\
		docker rmi registry.gitlab.com/inspiry/api;\
	fi
	docker build -t registry.gitlab.com/inspiry/api .
	docker push registry.gitlab.com/inspiry/api

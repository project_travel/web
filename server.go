package server

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/evalphobia/logrus_fluent"

	"bitbucket.org/seka7/web/booking"
	"bitbucket.org/seka7/web/localization"
	"bitbucket.org/seka7/web/skyscanner"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/mgo.v2"
)

var Issuer string

func initLogger(level logrus.Level) *logrus.Logger {
	llog := logrus.StandardLogger()
	llog.Out = os.Stderr
	llog.Level = logrus.DebugLevel
	llog.Formatter = &logrus.TextFormatter{
		DisableTimestamp: false,
		FullTimestamp:    true,
	}

	return llog
}

func addFluentHook(fl Fluent) {
	tmp := strings.Split(fl.Address, ":")
	port, _ := strconv.Atoi(tmp[1])
	hook, err := logrus_fluent.New(tmp[0], port)
	if err == nil {
		// set custom fire level
		hook.SetLevels([]logrus.Level{
			logrus.InfoLevel,
			logrus.DebugLevel,
		})

		// set static tag
		hook.SetTag(fl.Tag)

		logrus.AddHook(hook)
	}
}

type Config struct {
	Address string `json:"address"`
	// ApiPath string `json:"api_path"`
	Paths map[string]map[string]string

	MongoAddress    string          `json:"mongo"`
	Secret          string          `json:"secret"`
	RefreshSecret   string          `json:"refreshSecret"`
	SigningMethod   string          `json:"signingMethod"`
	Issuer          string          `json:"issuer"`
	SwaggerConf     string          `json:"swagger"`
	SwaggerIndex    string          `json:"swagger-ui"`
	InstagramConfig InstagramConfig `json:"instagram"`
	TwitterConfig   TwitterConfig   `json:"twitter"`
	FacebookConfig  FacebookConfig  `json:"facebook"`
	RabbitAddr      string          `json:"rabbit"`
	Storage         Storage         `json:"storage"`
	Pusher          Pusher          `json:"pusher"`
	TaskModule      TaskModule      `json:"tasks"`
	Mail            MailClient      `json:"mail"`
	LocalizationDir string          `json:"localization"`
	Logger          Fluent          `json:"fluent"`
	Booking         struct {
		Aid  string `json:"aid"`
		Dump string `json:"dump"`
	} `json:"booking"`
	Skyscanner struct {
		Key string `json:"key"`
	} `json:"skyscanner"`
}

type Fluent struct {
	Address string `json:"address"`
	Tag     string `json:"tag"`
}

type Server struct {
	Address string
	Paths   map[string]map[string]string
	// LocalizedPaths     map[string]map[string]string
	MongoAddress       string
	AccessTokenConfig  JWTConfig
	RefreshTokenConfig JWTConfig
	Hosts              map[string]*echo.Echo

	Rabbit  *Rabbit
	Storage *Storage

	DB         *DB
	MailClient *MailClient
	Pusher     *Pusher
	TaskModule *TaskModule

	InstagramConfig *InstagramConfig
	TwitterConfig   *TwitterConfig
	FacebookConfig  *FacebookConfig

	Booking    *booking.Ad
	Skyscanner *skyscanner.Ad

	logger     *logrus.Logger
	translator *localization.Translations
}

func CreateServer(filename string, dev bool) (*Server, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	config := new(Config)
	if err := json.Unmarshal(data, config); err != nil {
		return nil, err
	}

	logger := initLogger(logrus.DebugLevel)
	addFluentHook(config.Logger)

	logger.Info("Config successfully read!")

	Issuer = config.Issuer

	if config.SigningMethod == "" {
		config.SigningMethod = "HS256"
	}
	s := &Server{
		Address:      config.Address,
		Paths:        config.Paths,
		Hosts:        make(map[string]*echo.Echo),
		MongoAddress: config.MongoAddress,
		Storage:      &config.Storage,
		Pusher:       &config.Pusher,
		MailClient:   &config.Mail,
		TaskModule:   &config.TaskModule,
		AccessTokenConfig: JWTConfig{
			SigningKey:    []byte(config.Secret),
			SigningMethod: config.SigningMethod,
			ContextKey:    "access_token",
		},
		RefreshTokenConfig: JWTConfig{
			SigningKey:    []byte(config.RefreshSecret),
			SigningMethod: config.SigningMethod,
			ContextKey:    "refresh_token",
		},
		InstagramConfig: &config.InstagramConfig,
		TwitterConfig:   &config.TwitterConfig,
		FacebookConfig:  &config.FacebookConfig,
		logger:          logger,
	}

	s.Pusher.Server = s
	s.TaskModule.Server = s

	logger.WithField("address", s.MongoAddress).Info("Init mongodb connection...")
	s.DB, err = s.InitDB(s.MongoAddress, logger)
	if err != nil {
		return nil, err
	}
	logger.Info("Mongodb connection established!")

	if dev {
		return s, nil
	}

	logger.Info("Loading localization files...")
	s.translator, err = localization.Init(config.LocalizationDir)
	if err != nil {
		return nil, err
	}
	logger.Info("Localization files loaded!")

	logger.Info("Connecting to smtp server...")
	if err := s.MailClient.Init(s); err != nil {
		return nil, err
	}
	logger.Info("Successfully connected!")

	s.TwitterConfig.InitClient()

	logger.Info("Loading booking.com configs and files")
	s.Booking, err = booking.Init(config.Booking.Aid, config.Booking.Dump)
	if err != nil {
		return nil, err
	}
	logger.Info("booking.com configs and files are added!")

	s.Skyscanner = skyscanner.Init(config.Skyscanner.Key)

	logger.Info("Init rabbit connection...")
	// Init rabbit connection and channel
	rabbit, err := NewRabbit(config.RabbitAddr)
	if err != nil {
		return nil, err
	}
	logger.Info("Rabbit connection established!")

	rabbit.Server = s
	s.Rabbit = rabbit

	go func() {
		for {
			if err := s.Rabbit.Done(); err != nil {
				logger.WithError(err).Error("Rabbit mq done channel is closed")
				time.Sleep(time.Second * 10)
			}
		}
	}()

	return s, nil
}

func (s *Server) Run() error {
	// Fill pools
	initPools()

	if err := s.DB.AddCollection("users", []mgo.Index{mgo.Index{
		Key: []string{"firstName", "lastName", "nickName"},
	}, mgo.Index{
		Key:    []string{"username"},
		Unique: true,
	}, mgo.Index{
		Key:    []string{"email"},
		Unique: true,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("unverifiedUsers", []mgo.Index{mgo.Index{
		Key: []string{"user.username", "_id"},
	}, mgo.Index{
		Key:    []string{"user.username"},
		Unique: true,
	}, mgo.Index{
		Key:    []string{"user.email"},
		Unique: true,
	}, mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Hour * 12,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("notifications", []mgo.Index{mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Hour * 24 * 7,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("unverifiedEmails", []mgo.Index{mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Hour * 24,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("tokens", []mgo.Index{mgo.Index{
		Key: []string{"app", "username", "device"},
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("trips", []mgo.Index{mgo.Index{
		Key: []string{"_id", "owners", "visible"},
	}, mgo.Index{
		Key: []string{"start", "end", "_id"},
	}, mgo.Index{
		Key: []string{"$text:name"},
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("posts", []mgo.Index{mgo.Index{
		Key: []string{"_id", "owner", "visible"},
	}, mgo.Index{
		Key: []string{"created", "_id"},
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("creds", nil); err != nil {
		return err
	}

	if err := s.DB.AddCollection("apps", nil); err != nil {
		return err
	}

	if err := s.DB.AddCollection("likes", []mgo.Index{mgo.Index{
		Key:    []string{"username", "tripId"},
		Unique: true,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("polls", nil); err != nil {
		return err
	}

	if err := s.DB.AddCollection("emails", nil); err != nil {
		return err
	}

	if err := s.DB.AddCollection("tasks", []mgo.Index{mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Duration(s.TaskModule.Time) * time.Minute,
	}}); err != nil {
		return err
	}

	if err := s.DB.AddCollection("requestedTrips", []mgo.Index{mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Hour,
	}}); err != nil {
		return err
	}

	// API section
	api := echo.New()
	// Middleware
	api.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.POST, echo.PUT, echo.DELETE},
	}))
	// api.Use(ErrorExpander)
	// api.Use(middleware.Logger())
	api.Use(Logger(s.logger))
	api.HTTPErrorHandler = errorHandler
	api.Use(middleware.Recover())

	s.Hosts["api"] = api
	// Redirect to main site
	api.GET("/", func(c echo.Context) error {
		return c.Redirect(http.StatusFound, "http://inspiry.me")
	})

	// API for working with users trips

	api.GET("/trips", s.getTrips, s.meChain()...)
	api.GET("/trips/:id", s.getTrip, s.meChain()...)
	api.PUT("/trips/:id", s.updateTrip, s.secureChain()...)
	api.POST("/trips/:id/likes", s.likeTrip, s.secureChain()...)
	api.DELETE("/trips/:id/likes", s.dislikeTrip, s.secureChain()...)
	api.GET("/trips/:id/likes", s.getLikesTrip, s.meChain()...)

	api.GET("/trips/:id/comments", s.getComments, s.meChain()...)
	api.POST("/trips/:id/comments", s.postComment, s.secureChain()...)
	api.PUT("/trips/:id/comments/:commentId", s.updateComment, s.secureChain()...)
	api.DELETE("/trips/:id/comments/:commentId", s.deleteComment, s.secureChain()...)

	api.GET("/trips/:id/posts", s.getPosts, s.meChain()...)
	api.PUT("/trips/:id/posts/:postId", s.updatePost, s.secureChain()...)
	api.GET("/trips/:id/ads", s.getAdLinks, s.meChain()...)

	// API for working with users

	api.POST("/users", s.createUser)
	api.GET("/users", s.getUsers, s.meChain()...)

	api.GET("/users/:username", s.getUser, s.meChain()...)
	api.DELETE("/users/:username", s.deleteUser, s.secureChain()...)
	api.PUT("/users/:username", s.updateUser, s.secureChain()...)
	api.PUT("/users/:username/trips", s.harvestPosts, s.secureChain()...)
	api.GET("/users/:username/trips", s.getTrips, s.meChain()...)
	api.POST("/users/:username/avatar", s.uploadUserAvatar, s.secureChain()...)
	api.DELETE("/users/:username/avatar", s.deleteUserAvatar, s.secureChain()...)
	api.POST("/users/:username/background", s.uploadUserBackground, s.secureChain()...)
	api.DELETE("/users/:username/background", s.deleteUserBackground, s.secureChain()...)
	api.PUT("/users/:username/password", s.changePassword, s.secureChain()...)
	api.PUT("/users/:username/email", s.changeEmail, s.secureChain()...)
	api.GET("/users/:username/verify/:code", s.verifyUser)
	api.GET("/users/:username/verify_email/:code", s.verifyNewEmail)

	api.GET("/users/:username/following", s.getFollowing, s.meChain()...)
	api.GET("/users/:username/users/recommendations", s.getPossibleFriends, s.secureChain()...)

	api.GET("/users/:username/followers", s.getFollowers, s.meChain()...)
	api.POST("/users/:username/following/:stranger", s.followUser, s.secureChain()...)
	api.DELETE("/users/:username/following/:stranger", s.unfollowUser, s.secureChain()...)

	api.GET("/users/:username/blacklist/users", s.getBlacklistedUsers, s.secureChain()...)
	api.GET("/users/:username/blacklist/trips", s.getBlacklistedTrips, s.secureChain()...)
	api.GET("/users/:username/blacklist/posts", s.getBlacklistedPosts, s.secureChain()...)
	api.POST("/users/:username/blacklist", s.addToBlacklist, s.secureChain()...)
	api.DELETE("/users/:username/blacklist", s.removeFromBlacklist, s.secureChain()...)

	// Get linked social medias
	api.GET("/users/:username/social_medias", s.getUserSocialMedias, s.meChain()...)
	// Remove linked social medias
	api.DELETE("/users/:username/social_medias/:name", s.removeUserSocialMedias, s.secureChain()...)

	// Get user's feed
	api.GET("/users/:username/feed", s.getTripsFeed, s.secureChain()...)

	// Get trips, which're recommended for user
	api.GET("/users/:username/trips/recommendations", s.getTripsRecommendations, s.secureChain()...)

	api.GET("/users/:username/notifications", s.getNotifications, s.meChain()...)

	// Unbind social medias
	// api.DELETE("/users/:username/instagram", s.deleteInstagram, s.secureChain()...)
	// api.DELETE("/users/:username/twitter", s.deleteTwitter, s.secureChain()...)

	// API for working with tokens

	// Get access token
	api.POST("/tokens/access", s.getAccessToken)

	// Get refresh token
	api.POST("/tokens/refresh", s.getRefreshToken)

	// Instagram section

	api.GET("/tokens/instagram", s.authorizeInstagram, JWTWithConfig(s.AccessTokenConfig, false))
	uInst, err := url.Parse(s.InstagramConfig.RedirectURL)
	if err != nil {
		return err
	}
	api.GET(uInst.Path, s.callbackInstagram)

	// Facebook section

	api.GET("/tokens/facebook", s.authorizeFacebook, JWTWithConfig(s.AccessTokenConfig, false))
	uFace, err := url.Parse(s.FacebookConfig.RedirectURL)
	if err != nil {
		return err
	}
	api.GET(uFace.Path, s.callbackFacebook)

	// Twitter section

	api.GET("/tokens/twitter", s.authorizeTwitter, JWTWithConfig(s.AccessTokenConfig, false))
	uTwit, err := url.Parse(s.TwitterConfig.RedirectURL)
	if err != nil {
		return err
	}
	api.GET(uTwit.Path, s.callbackTwitter)

	// API for general purposes
	api.POST("/password", s.remindPassword)
	api.POST("/verification", s.resendVerification)
	api.GET("/locale", s.getLocale)

	api.GET("/test/:username", s.testtest)

	api.POST("/support", s.supportHandler)
	api.Start(s.Address)

	return nil

}

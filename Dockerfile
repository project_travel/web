FROM alpine:latest
COPY ./main/web_docker /web_docker
COPY ./localization/languages /languages
COPY ./email /email
COPY ./booking/data.tsv /data.tsv
RUN apk add --no-cache ca-certificates
ENTRYPOINT ["./web_docker"]
VOLUME /config.json
EXPOSE 8000

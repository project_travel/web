package server

import (
	"encoding/json"

	"github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
)

type TripsUpdate struct {
	Id       string `json:"id"`
	Username string `json:"username"`
	Start    *Time  `json:"start,omitempty"`
	End      *Time  `json:"end,omitempty"`
	Update   bool   `json:"update"`
}

// DEBU[0030] {"username": "adan", "updated": [], "error": false, "id": "57c970a41d9cca85837f440e", "new": []}  fields.time=2016-09-02T12:28:55Z time=2016-09-02T12:28:55Z
type TripsUpdateResponse struct {
	Id            string   `json:"id"`
	Username      string   `json:"username"`
	Updated       []string `json:"updated"`
	New           []string `json:"new"`
	ErrorOccuried bool     `json:"error"`
}

type Rabbit struct {
	Conn   *amqp.Connection
	Chan   *amqp.Channel
	Queues map[string]*amqp.Queue

	Server *Server
}

func NewRabbit(addr string) (*Rabbit, error) {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return nil, err
	}
	r := &Rabbit{Conn: conn, Queues: make(map[string]*amqp.Queue)}
	r.Chan, err = conn.Channel()
	if err != nil {
		return nil, err
	}
	// r.ExchangeDeclare("data", "fanout")
	q, err := r.Chan.QueueDeclare(
		"data", // name
		true,   // durable
		false,  // delete when unused
		false,  // exclusive
		false,  // no-wait
		nil,    // arguments
	)
	if err != nil {
		return nil, err
	}
	r.Queues["data"] = &q
	return r, nil
}

func (r *Rabbit) FormTrips(request TripsUpdate) error {
	body, err := json.Marshal(request)
	if err != nil {
		return err
	}

	err = r.send("data", body)
	r.Server.logger.WithFields(logrus.Fields{
		"request": request,
		"error":   err,
	}).Info("Sending form trips request")
	return err
}

func (r *Rabbit) send(tag string, msg []byte) error {
	return r.Chan.Publish(
		"",                 // exchange
		r.Queues[tag].Name, // routing key
		false,              // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         msg,
		})
}

func (r *Rabbit) Done() error {
	q, err := r.Chan.QueueDeclare(
		"done", // name
		true,   // durable
		false,  // delete when unused
		false,  // exclusive
		false,  // no-wait
		nil,    // arguments
	)
	if err != nil {
		return err
	}

	if err := r.Chan.Qos(
		10,   // prefetch count
		0,    // prefetch size
		true, // global
	); err != nil {
		return err
	}

	msgs, err := r.Chan.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		return err
	}

	r.Server.logger.Info("Rabbitmq done channel is listening!")

	for d := range msgs {
		response := new(TripsUpdateResponse)
		r.Server.logger.Debug(string(d.Body))
		if err := json.Unmarshal(d.Body, response); err != nil {
			r.Server.logger.WithError(err).Error("Can't unmarshal response from form trips worker")
			continue
		}
		r.Server.logger.WithFields(logrus.Fields{
			"response": response,
			"error":    err,
		}).Info("Got response from form trips worker")
		// Send push
		go func(response *TripsUpdateResponse) {
			if err := r.Server.Pusher.Send(
				response.Username,
				"trips",
				response.New,
				response.Updated,
				response.ErrorOccuried,
			); err != nil {
				r.Server.logger.WithError(err).Error("Can't send push")
			}
		}(response)
	}

	return nil

}

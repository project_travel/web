package server

import (
	"net/http"

	"github.com/labstack/echo"
)

type SupportMessage struct {
	Email   string `json:"email"`
	Message string `json:"message"`
}

func (s *Server) supportHandler(c echo.Context) error {
	msg := new(SupportMessage)
	if err := c.Bind(msg); err != nil {
		lang := getLanguage(c, s.translator)
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	go s.MailClient.SendToSupport(msg.Email, msg.Message)
	return c.JSON(http.StatusNoContent, nil)
}

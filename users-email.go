package server

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func (s *Server) verifyUser(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	code, err := getId(c, "code")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	user := new(struct {
		User User `json:"user"`
	})

	if err := s.DB.Collections["unverifiedUsers"].Get(bson.M{
		"user.username": c.Param("username"),
		"_id":           code,
	}, nil, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNotInRegistrationQueue.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Delete from tmp collection
	if err := s.DB.Collections["unverifiedUsers"].Delete(bson.M{"user.username": c.Param("username")}); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	if err := s.DB.Collections["tokens"].Update(
		bson.M{"username": c.Param("username")},
		bson.M{"$set": bson.M{"verified": true}}); err != nil {
		if !isNotFound(err) {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Move user to main collection
	if err := s.DB.Collections["users"].Create(user.User); err != nil {
		if mgo.IsDup(err) {
			return c.JSON(http.StatusConflict, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	return c.Redirect(http.StatusFound, s.GetPath("successfullRegistration", getLanguage(c, s.translator)))
}

func (s *Server) verifyNewEmail(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	code, err := getId(c, "code")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrInvalidContent.Localize(lang, s.translator))
	}

	email := new(struct {
		Id    bson.ObjectId `bson:"_id"`
		Email string        `bson:"email"`
	})

	if err := s.DB.Collections["unverifiedEmails"].Get(bson.M{
		"owner": c.Param("username"),
		"_id":   code,
	}, nil, email); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNotInRegistrationQueue.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Delete from tmp collection
	if err := s.DB.Collections["unverifiedEmails"].Delete(email); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	oldEmail := bson.M{}
	if err := s.DB.Collections["users"].FindAndModify(
		bson.M{"username": c.Param("username")},
		bson.M{"email": 1},
		bson.M{"$set": bson.M{"email": email.Email}},
		false,
		&oldEmail,
	); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Send email to old email address
	go func() {
		if err := s.MailClient.SendChangeEmailNotification(
			oldEmail["email"].(string),
			c.Param("username"),
			email.Email,
			getLanguage(c, s.translator),
		); err != nil {
			s.logger.WithError(err).Error("Can't send mail")
		}
	}()

	return c.Redirect(http.StatusFound, "http://inspiry.me/login")
}

func (s *Server) resendVerification(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	email := new(struct {
		Email string `json:"email"`
	})
	if err := c.Bind(email); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	user := new(UnverifiedUser)
	if err := s.DB.Collections["unverifiedUsers"].Get(bson.M{"user.email": email.Email}, bson.M{"_id": 1, "user": 1}, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNotInRegistrationQueue.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Send email with code
	go func() {
		lang := getLanguage(c, s.translator)
		if err := s.MailClient.SendVerification(user.User.Email, user.User.Username, user.Id.Hex(), lang); err != nil {
			fmt.Println("Email error ->", err)
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) changeEmail(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	email := new(struct {
		Email string `json:"email" bson:"email"`
	})

	id := bson.NewObjectId()

	if err := c.Bind(email); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	if email.Email != "" && !isValidEmail(email.Email) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "email must contain @ symbol"))
	}

	err := s.DB.Collections["users"].Get(email, bson.M{"_id": 1}, nil)
	if err == nil {
		return c.JSON(http.StatusBadRequest, ErrEmailTaken.Localize(lang, s.translator))
	}
	if err != nil && !isNotFound(err) {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	if err := s.DB.Collections["unverifiedEmails"].Create(bson.M{
		"_id":     id,
		"email":   email.Email,
		"owner":   username,
		"created": NewTime(time.Now()),
	}); err != nil {
		if mgo.IsDup(err) {
			return c.JSON(http.StatusBadRequest, ErrEmailTakenInQueue.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	// Send email to new email address
	go func() {
		if err := s.MailClient.SendConfirmEmail(email.Email, username, id.Hex(), getLanguage(c, s.translator)); err != nil {
			s.logger.WithError(err).Error("Can't send mail")
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) remindPassword(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	email := new(struct {
		Email string `json:"email"`
	})
	if err := c.Bind(email); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	user := new(User)
	if err := s.DB.Collections["users"].Get(bson.M{"email": email.Email}, bson.M{"_id": 0, "password": 1, "username": 1}, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	password, err := base64.StdEncoding.DecodeString(user.Password)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Send email with password
	go func() {
		if err := s.MailClient.SendPassword(email.Email, user.Username, string(password), getLanguage(c, s.translator)); err != nil {
			s.logger.WithError(err).Error("Can't send mail")
		}
	}()
	return c.JSON(http.StatusNoContent, nil)
}

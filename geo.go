package server

import "sync"

var coordPool = &sync.Pool{
	New: func() interface{} { return [][]float64{} },
}

var polygonPool = &sync.Pool{
	New: func() interface{} { return [][][]float64{} },
}

type Geo struct {
	Type        string      `json:"type" bson:"type"`
	Coordinates interface{} `json:"coordinates" bson:"coordinates"`
}

// Correct corrects geo fields to fit GeoJSON standarts
func (g *Geo) Correct() *Geo {
	if g == nil {
		return nil
	}
	switch coord := g.Coordinates.(type) {
	case []interface{}:
		if g.Type == "Polygon" {
			res := polygonPool.Get().([][][]float64)
			for j := 0; j < len(coord); j++ {
				tmp := coord[j].([]interface{})
				floats := coordPool.Get().([][]float64)
				for _, v := range tmp {
					floats = append(floats, []float64{v.([]interface{})[0].(float64), v.([]interface{})[1].(float64)})
				}
				if floats[0][0] != floats[len(floats)-1][0] || floats[0][1] != floats[len(floats)-1][1] {
					floats = append(floats, floats[0])
				}
				res = append(res, floats)
				coordPool.Put(floats[:0])
			}
			g.Coordinates = res
			polygonPool.Put(res[:0])
		}
	}
	return g
}

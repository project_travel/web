package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

type Storage struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

type StorageResponse struct {
	Url string `json:"url"`
}

// Creates a new file upload http request with optional extra params
func newFileUploadRequest(uri, paramName string, ff interface{}) (*http.Request, error) {
	var data []byte
	var filename string
	switch v := ff.(type) {
	case *multipart.FileHeader:
		file, err := v.Open()
		if err != nil {
			return nil, err
		}
		defer file.Close()
		fileContents, err := ioutil.ReadAll(file)
		if err != nil {
			return nil, err
		}
		data, filename = fileContents, v.Filename
		break
	case ImageUpload:
		data, filename = v.Data, v.Name
		break
	}

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, filename)
	if err != nil {
		return nil, err
	}
	part.Write(data)

	if err = writer.Close(); err != nil {
		return nil, err
	}
	request, err := http.NewRequest("POST", uri, body)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", writer.FormDataContentType())
	return request, nil
}

func (s *Storage) Send(file interface{}) (*StorageResponse, error) {
	request, err := newFileUploadRequest(s.Address, "file", file)
	if err != nil {
		return nil, err
	}
	request.SetBasicAuth(s.Username, s.Password)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("Wrong status code -> %v", response.StatusCode)
	}

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(response.Body); err != nil {
		return nil, err
	}

	res := new(StorageResponse)
	if err := json.Unmarshal(buf.Bytes(), res); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *Storage) Delete(url string) error {
	request, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}
	request.SetBasicAuth(s.Username, s.Password)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusNoContent {
		return fmt.Errorf("Wrong status code -> %v", response.StatusCode)
	} else {
		return nil
	}
}

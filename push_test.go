package server

import (
	"fmt"
	"testing"

	"bitbucket.org/seka7/web/localization"
)

func TestSendPush(t *testing.T) {

	// server, err := CreateServer("./main/test.json", true)
	// if err != nil {
	// 	t.FailNow()
	// }

	// go func(server *Server) {
	// 	if err := server.Run(); err != nil {
	// 		fmt.Println(err)
	// 		t.Error(err)
	// 		// t.FailNow()
	// 	}
	// }(server)

	// fmt.Println("Waiting server to start")
	// time.Sleep(time.Second * 7)
	// fmt.Println("Stor waiting")

	pusher := &Pusher{
		Id:      "7ca18f59-7578-4a6b-92ae-ff2297149ff4",
		Secret:  "NTk0ZGNkOGEtNTcwZi00ZGFmLWIzZDktYmEzOWQxMjM5OGZl",
		Address: "https://onesignal.com/api/v1/notifications",
		// Server:  server,
	}

	push := Push{pusher.Id, []string{"c9284d47-2732-47af-8c4e-78d3adea4e2f"}, map[string]interface{}{"type": "newLike", "tripId": "57ad833230890f0dc3bbceef"}, map[string]string{"en": "Hello!"}}
	if err := pusher.TestPush(push); err != nil {
		t.FailNow()
	}

}

func TestCreateNotification(t *testing.T) {
	local, err := localization.Init("./localization/languages")
	if err != nil {
		t.FailNow()
	}

	// {App:7ca18f59-7578-4a6b-92ae-ff2297149ff4 Devices:[c9284d47-2732-47af-8c4e-78d3adea4e2f] Data:map[type:newLike tripId:5815a5b25be1db0001852e19 tripName:RU, 2016 username:adanilyak] Message:map[]}

	// pusher := &Pusher{
	// 	Id:      "7ca18f59-7578-4a6b-92ae-ff2297149ff4",
	// 	Secret:  "NTk0ZGNkOGEtNTcwZi00ZGFmLWIzZDktYmEzOWQxMjM5OGZl",
	// 	Address: "https://onesignal.com/api/v1/notifications",
	// 	// Server:  server,
	// }

	notif, err := createNotificationMessage(map[string]interface{}{
		"type":     "newLike",
		"tripId":   "5815a5b25be1db0001852e19",
		"tripName": "RU, 2016",
		"username": "adanilyak",
	}, local)

	if err != nil || notif == nil {
		t.FailNow()
	} else {
		fmt.Println(notif)
	}

	// push := Push{pusher.Id, []string{"c9284d47-2732-47af-8c4e-78d3adea4e2f"}, map[string]interface{}{"type": "newLike", "tripId": "57ad833230890f0dc3bbceef"}, map[string]string{"en": "Hello!"}}
	// if err := pusher.TestPush(push); err != nil {
	// 	t.FailNow()
	// }

}

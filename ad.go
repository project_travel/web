package server

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/seka7/web/localization"

	"github.com/labstack/echo"
)

type AdsTranslation struct {
	Elements []interface{}
}
type GeoTranslate interface {
	GetCity() string
	GetCountry() string
	GetCityId() string
	GetCountryId() string
	SetCity(string)
	SetCountry(string)
}

func (s *Server) getAdLinks(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	username := getUsernameFromToken(c)

	search := bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
	}

	trip := new(Trip)
	if err := s.DB.Collections["trips"].Get(search, bson.M{"cities": 1}, trip); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoTrip)
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal)
		}
	}

	req := make([]interface{}, len(trip.Cities))

	for i, v := range trip.Cities {
		trip.Cities[i].Name, _ = s.translator.T(v.Id, "en", false)
		trip.Cities[i].CountryName, _ = s.translator.T(v.CountryId, "en", false)
		req[i] = v
	}

	skyscannerAds := s.Skyscanner.Get(req, c, nil, nil)

	bookingAds := s.Booking.Get(req)

	adsTranslation := &AdsTranslation{make([]interface{}, len(bookingAds)+len(skyscannerAds))}
	for _, v := range skyscannerAds {
		for j, vv := range bookingAds {
			if v.GetCity() == vv.GetCity() {
				bookingAds[j].SetCityId(v.GetCityId())
			}
		}
	}

	for i, v := range bookingAds {
		adsTranslation.Elements[i] = v
	}
	for i, v := range skyscannerAds {
		adsTranslation.Elements[i+len(bookingAds)] = v
	}

	adsTranslation.Translate(lang, s.translator)

	return c.JSON(http.StatusOK, adsTranslation.Elements)
}

// Translate translates country and city to provided language
func (this AdsTranslation) Translate(language string, f localization.TInterface) {
	for _, tmp := range this.Elements {
		fmt.Println(tmp)
		ad, ok := tmp.(GeoTranslate)
		if !ok {
			continue
		}
		// Translate country
		if ad.GetCountryId() != "" {
			if tmp, err := f.T(ad.GetCountryId(), language, false); err == nil {
				ad.SetCountry(tmp)
			}
		} else {
			if tmp, err := f.T(ad.GetCountry(), language, true); err != nil {
				// ad.SetCountry(tmp)
				continue
			} else {
				// From code to country
				if tmp, err := f.T(tmp, language, false); err == nil {
					ad.SetCountry(tmp)
				}
			}
		}
		// Translate city
		if ad.GetCityId() != "" {
			if tmp, err := f.T(ad.GetCityId(), language, false); err == nil {
				ad.SetCity(stripCityName(tmp))
			}
		} else {
			if tmp, err := f.T(ad.GetCity(), "en", true); err != nil {
				continue
			} else {
				// From code to city
				if tmp, err := f.T(tmp, language, false); err == nil {
					ad.SetCity(stripCityName(tmp))
				}
			}
		}
	}

	return
}

package server

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strconv"
	"sync"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

func (s *Server) secureChain() []echo.MiddlewareFunc {
	return []echo.MiddlewareFunc{JWTWithConfig(s.AccessTokenConfig, false), s.CheckUserAccess}
}

func (s *Server) meChain() []echo.MiddlewareFunc {
	return []echo.MiddlewareFunc{JWTWithConfig(s.AccessTokenConfig, true)}
}

// CheckUsersAccess checks if user have access to object
func (s *Server) CheckUserAccess(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Param("username") == "me" {
			c.Set("username", getUsernameFromToken(c))
		} else {
			c.Set("username", c.Param("username"))
		}
		return next(c)
	}
}

func Logger(logger *logrus.Logger) echo.MiddlewareFunc {
	// Initiate pool
	bufferPool := sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 256))
		},
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			req := c.Request()
			res := c.Response()
			start := time.Now().UTC()
			if err = next(c); err != nil {
				c.Error(err)
			}
			stop := time.Now().UTC()
			buf := bufferPool.Get().(*bytes.Buffer)
			buf.Reset()
			defer bufferPool.Put(buf)

			b := req.Header.Get(echo.HeaderContentLength)
			if b == "" {
				b = "0"
			}
			logger.WithFields(logrus.Fields{
				"remote_ip":     c.RealIP(),
				"method":        req.Method,
				"uri":           req.URL,
				"status":        res.Status,
				"latency_human": stop.Sub(start).String(),
				"latency":       strconv.FormatInt(stop.Sub(start).Nanoseconds()/1000, 10),
				"bytes_in":      b,
				"bytes_out":     strconv.FormatInt(res.Size, 10),
				"username":      getUsernameFromToken(c),
			}).Info("Request")
			return
		}
	}
}

func errorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	msg := http.StatusText(code)
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		msg = he.Message
	}
	if !c.Response().Committed {
		if c.Request().Method == echo.HEAD {
			c.NoContent(code)
		} else {
			switch code {
			case http.StatusNotFound:
				c.JSON(http.StatusBadRequest, Error{404, msg, msg})
			case http.StatusBadRequest:
				c.JSON(http.StatusBadRequest, Error{ErrBadRequest.Code, msg, ErrBadRequest.MsgCode})
			case http.StatusUnauthorized:
				c.JSON(http.StatusUnauthorized, Error{ErrInvalidToken.Code, msg, ErrInvalidToken.MsgCode})
			case http.StatusUnsupportedMediaType:
				c.JSON(http.StatusUnsupportedMediaType, Error{ErrInvalidContent.Code, msg, ErrInvalidContent.MsgCode})
			case http.StatusMethodNotAllowed:
				c.JSON(http.StatusMethodNotAllowed, Error{ErrInvalidContent.Code, msg, ErrInvalidContent.MsgCode})
			case http.StatusInternalServerError:
				c.JSON(http.StatusInternalServerError, Error{ErrInternal.Code, msg, ErrInternal.MsgCode})
			default:
				c.JSON(code, Error{666, msg, msg})
			}
		}
	}
}

func ErrorExpander(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		rec := httptest.NewRecorder()
		w := c.Response().Writer()
		c.Response().SetWriter(rec)
		err := next(c)
		c.Response().SetWriter(w)
		// Check, that Content type is application/json. It's set when c.JSON(...) executes.
		if c.Response().Header().Get(echo.HeaderContentType) != echo.MIMEApplicationJSONCharsetUTF8 {
			// If it's not, we need to transform error in Error type.
			message := rec.Body.String()
			code := c.Response().Status
			// logrus.Println("code ->", code)
			switch code {
			case http.StatusBadRequest:
				return c.JSON(http.StatusBadRequest, Error{ErrBadRequest.Code, message, ErrBadRequest.MsgCode})
			case http.StatusUnauthorized:
				return c.JSON(http.StatusUnauthorized, Error{ErrInvalidToken.Code, message, ErrInvalidToken.MsgCode})
			case http.StatusUnsupportedMediaType:
				return c.JSON(http.StatusUnsupportedMediaType, Error{ErrInvalidContent.Code, message, ErrInvalidContent.MsgCode})
			case http.StatusMethodNotAllowed:
				return c.JSON(http.StatusMethodNotAllowed, Error{ErrInvalidContent.Code, message, ErrInvalidContent.MsgCode})
				// default:
				// 	if code < http.StatusMultipleChoices || code > http.StatusTemporaryRedirect {
				// 		logrus.Println("Unsupported status ->", code)
				// 	}
			}
		}

		w.Write(rec.Body.Bytes())

		return err
	}
}

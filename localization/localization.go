package localization

import (
	"errors"
	"io/ioutil"
	"path"

	"github.com/labstack/echo"
	"github.com/nicksnyder/go-i18n/i18n"
	"golang.org/x/text/language"
)

type Translations struct {
	Matcher language.Matcher
	tags    []string
}

type TranslationInterface interface {
	Translate(string, TInterface)
}

type TInterface interface {
	T(string, string, bool, ...interface{}) (string, error)
}

func (this *Translations) Translate(data TranslationInterface, lang string) {
	data.Translate(lang, this)
}

func (this *Translations) Languages() []string {
	return this.tags
}

func Init(p string) (*Translations, error) {
	// Supported languages, furs value is default
	translations := &Translations{Matcher: language.NewMatcher([]language.Tag{
		language.English,
		// language.German,
		// language.Italian,
		// language.French,
		// language.Spanish,
		language.Russian,
	}), tags: []string{"en", "ru"}}
	// Load translations in memmory
	files, err := ioutil.ReadDir(p)
	if err != nil {
		return nil, err
	}
	for _, f := range files {
		if []byte(f.Name())[0] == '.' {
			continue
		}
		if err := i18n.LoadTranslationFile(path.Join(p, f.Name())); err != nil {
			return nil, err
		}
	}
	return translations, nil
}

// Parses Accept-Language header and returns best matching language
func (tr Translations) GetLanguageFromHeader(c echo.Context) string {
	t, _, _ := language.ParseAcceptLanguage(c.Request().Header.Get("Accept-Language"))
	// We ignore the error: the default language will be selected for t == nil.
	tag, _, _ := tr.Matcher.Match(t...)
	// fmt.Printf("%5v (t: %6v; q: %3v; err: %v)\n", tag, t, q, err)
	return tag.String()
}

// Returns translate function
func (tr Translations) TransFunc(key string, reverse bool) (i18n.TranslateFunc, error) {
	if reverse {
		return i18n.Tfunc(key + "-rev")
	} else {
		return i18n.Tfunc(key)
	}
}

func (tr Translations) T(v, key string, reverse bool, data ...interface{}) (string, error) {
	T, err := tr.TransFunc(key, reverse)
	if err == nil {
		var tmp string
		if len(data) != 0 {
			tmp = T(v, data[0])
		} else {
			tmp = T(v)
		}
		// Check that value changed
		if tmp != v {
			return tmp, nil
		}
	}

	//If can't translate for this key, try others
	for _, lang := range tr.tags {
		T, err := tr.TransFunc(lang, reverse)
		if err == nil {
			tmp := T(v)
			// Check that value changed
			if tmp != v {
				return tmp, nil
			}
		}
	}
	return "", errors.New("Can't translate")
}

func (tr Translations) TAll(key string, data interface{}, reverse bool) map[string]string {
	res := make(map[string]string)
	for _, lang := range tr.tags {
		T, err := tr.TransFunc(lang, reverse)
		if err == nil {
			tmp := T(key, data)
			// Check that value changed
			if tmp != key {
				res[lang] = tmp
			}
		}
	}
	return res
}

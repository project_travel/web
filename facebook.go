package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type FacebookConfig struct {
	Id          string `json:"id"`
	Secret      string `json:"secret"`
	CodeURL     string `json:"codeUrl"`
	TokenURL    string `json:"tokenUrl"`
	RedirectURL string `json:"redirectUrl"`
}

type FacebookUser struct {
	Id             string `json:"id" bson:"id"`
	Username       string `json:"username" bson:"username"`
	FullName       string `json:"full_name" bson:"fullName"`
	ProfilePicture string `json:"profile_picture" bson:"profilePicture"`
}

type FacebookAccessTokenResponse struct {
	Token   string `json:"access_token" bson:"accessToken"`
	Type    string `json:"token_type" bson:"-"`
	Expires int    `json:"expires_in" bson:"-"`
}

type FacebookUserData struct {
	Token string        `json:"access_token" bson:"accessToken"`
	User  InstagramUser `json:"user" bson:"user"`
}

// Redirect user to instagram site to auth
func (s *Server) authorizeFacebook(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	// What user sends request
	userToken := c.Get("access_token").(*jwt.Token)
	claims := userToken.Claims.(jwt.MapClaims)
	username := claims["username"].(string)

	// Get redirect url
	ru := c.QueryParam("redirect_url")
	if ru == "" {
		ru = "/"
	}

	// Pass him to auth facebook URL
	u, err := url.Parse("https://www.facebook.com/dialog/oauth")
	log.Println(err)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	q := u.Query()
	q.Set("client_id", s.FacebookConfig.Id)
	q.Set("response_type", "code")
	q.Set("redirect_uri", s.FacebookConfig.RedirectURL)
	// Need to know user followers and etc
	q.Set("scope", "user_friends,user_photos,user_posts")
	// TODO: check if it works without this step
	q.Set("state", fmt.Sprintf("%s,%s",
		username, base64.URLEncoding.EncodeToString([]byte(ru))))
	u.RawQuery = q.Encode()
	// Right redirect URL in u
	if c.QueryParam("no_redirect") == "true" {
		return c.JSON(http.StatusOK, CustomRedirect{u.String()})
	} else {
		return c.Redirect(http.StatusFound, u.String())
	}
}

func (s *Server) callbackFacebook(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	u := c.Request().URL
	// u, err := url.Parse(
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	// }
	log.Println("URL from facebook ->", u)
	q := u.Query()
	code := q.Get("code")

	// Get state parametres from URL.
	tmp := strings.Split(q.Get("state"), ",")
	if len(tmp) != 2 {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	username, ru := tmp[0], tmp[1]

	// There's no code in URL if user denied to provide access token
	if code == "" {
		msg := fmt.Sprintf("%s: %s",
			q.Get("error_reason"), q.Get("error_description"))
		log.Println("Msg ->", msg)
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, msg}, ru)
	}
	// Exchange code for access token
	token, err := s.getFacebookAccessToken(username, code)
	if err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusBadRequest, err.Error()}, ru)
	}
	search := bson.M{"username": username}
	query := bson.M{"$set": bson.M{"facebook": token}}
	if err := s.DB.Collections["users"].Update(search, query); err != nil {
		return redirectAfterAuth(c, &AuthError{http.StatusInternalServerError, err.Error()}, ru)
	}
	return redirectAfterAuth(c, nil, ru)
}

func (s *Server) getFacebookAccessToken(username, code string) (*FacebookAccessTokenResponse, error) {
	// Pass him to auth facebook URL
	u, err := url.Parse("https://graph.facebook.com/v2.3/oauth/access_token")
	if err != nil {
		return nil, err
	}
	q := u.Query()
	q.Set("client_id", s.FacebookConfig.Id)
	q.Set("client_secret", s.FacebookConfig.Secret)
	q.Set("redirect_uri", s.FacebookConfig.RedirectURL)
	q.Set("code", code)
	u.RawQuery = q.Encode()

	response, err := http.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	buf := bytes.Buffer{}
	if _, err := buf.ReadFrom(response.Body); err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		codeErr := new(CodeError)
		if err := json.Unmarshal(buf.Bytes(), codeErr); err != nil {
			return nil, err
		}
		return nil, errors.New(codeErr.Error.Message)
	}
	token := new(FacebookAccessTokenResponse)
	if err := json.Unmarshal(buf.Bytes(), token); err != nil {
		return nil, err
	}
	log.Println("Marshal Token ->", token)
	return token, nil
}

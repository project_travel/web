package server

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type UpdatePost struct {
	Visible *bool `json:"visible,omitempty" bson:"visible"`
}

func (s *Server) getPosts(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	username := getUsernameFromToken(c)

	if err := s.DB.Collections["trips"].Get(bson.M{
		"_id":       id,
		"notShowTo": bson.M{"$ne": username},
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		}}, bson.M{"_id": 1}, nil); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	offset, size, order, err := setPagination(c, "created,_id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search := bson.M{
		"tripId":  id,
		"history": false,
		"$or": []bson.M{
			bson.M{"owner": username},
			bson.M{"visible": true},
		},
		"notShowTo": bson.M{"$ne": username},
	}

	sel := bson.M{
		"tripId":          0,
		"place":           0,
		"source.platform": 0,
		"source.id":       0,
		"source.likes":    0,
	}

	posts := make([]*Post, 0)

	if err := s.DB.Collections["posts"].GetAll(
		search,
		sel,
		offset,
		size,
		order,
		&posts,
	); err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, posts)
}

func (s *Server) updatePost(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	post := new(UpdatePost)
	if err := c.Bind(post); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator, err.Error()))
	}
	if post.Visible == nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "empty request"))
	}

	id, err := getId(c, "postId")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	username := getUsernameFromToken(c)
	search := bson.M{"_id": id, "owner": username, "history": false}
	update := bson.M{"$set": bson.M{"visible": *post.Visible, "modified": NewTime(time.Now())}}
	if err := s.DB.Collections["posts"].Update(search, update); err != nil {
		return s.DB.handleNotFoundError(c, err, "posts", bson.M{"_id": id}, ErrNoPost.Localize(lang, s.translator))
	}
	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) harvestPosts(c echo.Context) error {
	username := getUsernameFromToken(c)
	go func() {
		err1 := s.loadInstagramData(username)
		if err1 != nil {
			s.logger.WithError(err1).Warn("Load instagram data")
		}
		err2 := s.saveTweets(username)
		if err2 != nil {
			s.logger.WithError(err2).Warn("Load twitter data")
		}

		if err1 != nil && err2 != nil && err1.Error() == TaskOverload.Error() && err2.Error() == TaskOverload.Error() {
			s.Pusher.Send(
				username,
				"trips",
				[]string{},
				[]string{},
				true,
				true, // mark as a user error
			)
			return
		}

		request, err := s.formTripRequest(username, false)
		if err != nil {
			// log.Println("Can't form request error ->", err)
			s.logger.WithError(err).Warn("Can't form trip request")
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			s.logger.WithError(err).Warn("Can't send rabbit request")
			return
		}
	}()
	return c.JSON(http.StatusNoContent, nil)
}

type Date struct {
	Body *Time `bson:"date"`
}

func (this *Date) Date() *Time {
	if this == nil {
		return nil
	} else {
		return this.Body
	}
}

func (s *Server) formTripRequest(username string, newSocialMedia bool) (*TripsUpdate, error) {
	startQuery := []bson.M{
		bson.M{
			"$match": bson.M{
				"owner":  username,
				"tripId": bson.M{"$exists": true},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id":  "$owner",
				"date": bson.M{"$max": "$created"},
			},
		},
	}

	start := new(Date)

	if err := s.DB.Collections["posts"].Aggregate(startQuery, start); err != nil {
		fmt.Println("Min ->", err)
		if isNotFound(err) {
			start = nil
		} else {
			return nil, err
		}

	}

	if newSocialMedia {
		// No trips exists
		if start == nil {
			return &TripsUpdate{
				Id:       bson.NewObjectId().Hex(),
				Username: username,
				Start:    nil,
				End:      nil,
				Update:   false,
			}, nil
		} else {
			// From first post to last
			return &TripsUpdate{
				Id:       bson.NewObjectId().Hex(),
				Username: username,
				Start:    nil,
				End:      nil,
				Update:   true,
			}, nil

		}
	}

	endQuery := []bson.M{
		bson.M{
			"$match": bson.M{
				"owner": username,
			},
		},
		bson.M{
			"$group": bson.M{
				"_id":  "$owner",
				"date": bson.M{"$max": "$created"},
			},
		},
	}

	end := new(Date)

	if err := s.DB.Collections["posts"].Aggregate(endQuery, end); err != nil {
		if isNotFound(err) {
			fmt.Println("Max ->", err)
			end = nil
		} else {
			return nil, err
		}

	}

	var update bool
	// Update or not
	if start == nil {
		update = false
		end = nil
	} else {
		// No new unlabeled posts
		if end.Date().Equal(start.Date().Time) {
			return nil, nil
		}
		update = true
	}

	fmt.Println("Min ->", start.Date())
	fmt.Println("Max ->", end.Date())

	return &TripsUpdate{
		Id:       bson.NewObjectId().Hex(),
		Username: username,
		Start:    start.Date(),
		End:      end.Date(),
		Update:   update,
	}, nil
}

func (s *Server) testtest(c echo.Context) error {
	username := c.Param("username")
	go func() {
		if err := s.loadInstagramData(username); err != nil {
			log.Println("Load instagram data ->", err)
		}
		if err := s.saveTweets(username); err != nil {
			log.Println("Load tweets ->", err)
		}
		request, err := s.formTripRequest(username, true)
		if err != nil {
			log.Println("Can't form request error ->", err)
			return
		}
		// No new unlabeled posts
		if request == nil {
			return
		}
		if err := s.Rabbit.FormTrips(*request); err != nil {
			log.Println("Can't send rabbit request ->", err)
			return
		}
	}()
	return c.JSON(http.StatusNoContent, nil)

}

func (s *Server) testtest1(c echo.Context) error {
	username := c.Param("username")
	friends, err := s.getTwitterFriends(username)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	friends1, err := s.getInstagramFriends(username)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	fmt.Println(friends, friends1)
	return c.JSON(http.StatusOK, friends1)
}

package server

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

type ResponseToken struct {
	AccessToken  string `json:"accessToken,omitempty"`
	RefreshToken string `json:"refreshToken,omitempty"`
}

type RequestToken struct {
	Access   string `json:"accessToken"`
	Refresh  string `json:"refreshToken"`
	Login    string `json:"login"`
	Password string `json:"password"`
	GoogleId string `json:"googleId"`
	AppId    string `json:"app"`
	Device   string `json:"device"`
	PushId   string `json:"push"`
}

type RefreshToken struct {
	AppId    string `bson:"app"`
	Device   string `bson:"device"`
	Username string `bson:"username"`
	PushId   string `bson:"push"`
	Token    string `bson:"-"`
	Verified bool   `bson:"verified"`
}

type TokensDB struct {
	Server *Server
	coll   *mgo.Collection // соединение с MongoDB
}

func (s *Server) CreateRefreshToken(username, appId, device, pushId string, verified bool) (RefreshToken, error) {
	// Создаем токен
	token := jwt.New(jwt.SigningMethodHS256)

	// Задаем поля
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["app"] = appId
	claims["device"] = device
	claims["push"] = pushId
	claims["iss"] = Issuer
	claims["verified"] = verified

	// Подписываем токен и возвращаем его
	t, err := token.SignedString(s.RefreshTokenConfig.SigningKey)

	return RefreshToken{
		AppId:    appId,
		Device:   device,
		PushId:   pushId,
		Username: username,
		Token:    t,
		Verified: verified,
	}, err
}

func (s *Server) CreateAccessToken(username string) (string, error) {
	t := time.Now().UTC()

	// Creating token
	token := jwt.New(jwt.SigningMethodHS256)

	// Setting fields
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	exp := t.Add(time.Hour * 10).Unix()
	claims["exp"] = exp
	claims["iat"] = t.Unix()
	claims["iss"] = Issuer

	// If user has preffered language, set it in token
	lang := bson.M{}
	if err := s.DB.Collections["users"].Get(bson.M{
		"username": username,
		"language": bson.M{"$exists": true},
	}, bson.M{"language": 1}, &lang); err == nil {
		l, ok := lang["language"]
		if ok {
			ll, ok := l.(string)
			if ok {
				claims["lang"] = ll
			}
		}
	}

	// Signing and returning it
	return token.SignedString(s.AccessTokenConfig.SigningKey)
}

func (s *Server) getRefreshToken(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	request := new(RequestToken)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	user := User{
		Password: base64.StdEncoding.EncodeToString([]byte(request.Password)),
		Username: request.Login,
		AppId:    request.AppId,
		Device:   request.Device,
	}

	resp := new(User)
	if err := s.DB.Collections["users"].Get(user.VerifyQuery(), bson.M{"username": 1}, resp); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}
	username := resp.Username
	query := bson.M{
		"app":      request.AppId,
		"device":   request.Device,
		"username": username,
	}

	// Check if it's a registred app
	if err := s.DB.Collections["apps"].Get(bson.M{"secret": request.AppId}, nil, nil); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrInvalidAppId.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	token, err := s.CreateRefreshToken(user.Username, user.AppId, user.Device, user.PushId, true)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	if err := s.DB.Collections["tokens"].Update(query, bson.M{"$set": token}, true); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusCreated, ResponseToken{RefreshToken: token.Token})

}

func (s *Server) getAccessToken(c echo.Context) error {
	var username, refreshToken string
	lang := getLanguage(c, s.translator)
	request := new(RequestToken)
	if err := c.Bind(request); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}
	log.Println("Request for token ->", request)

	switch {
	case request.Refresh != "":
		token, err := jwt.Parse(request.Refresh, func(token *jwt.Token) (interface{}, error) {
			if token.Method.Alg() != s.RefreshTokenConfig.SigningMethod {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return s.RefreshTokenConfig.SigningKey, nil
		})

		if err != nil {
			return c.JSON(http.StatusBadRequest, ErrInvalidToken.Localize(lang, s.translator))
		}

		var appId, pushId, device string
		var verified bool

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			username = claims["username"].(string)
			device = claims["device"].(string)
			appId = claims["app"].(string)
			pushId = claims["push"].(string)
			verified = claims["verified"].(bool)
		} else {
			return c.JSON(http.StatusBadRequest, ErrInvalidToken.Localize(lang, s.translator))
		}

		if !verified {
			// If user not exists, return error
			if err := s.DB.Collections["users"].Get(bson.M{"username": username}, bson.M{"_id": 1}, nil); err != nil {
				if isNotFound(err) {
					return c.JSON(http.StatusBadRequest, ErrInvalidToken.Localize(lang, s.translator))
				} else {
					return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
				}
			}

			// Verify token
			t, err := s.CreateRefreshToken(username, appId, device, pushId, true)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}

			query := bson.M{
				"app":      appId,
				"device":   device,
				"username": username,
			}

			if err := s.DB.Collections["tokens"].Update(query, t, true); err != nil {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}

			refreshToken = t.Token
		}
		break
	case request.Access != "":
		token, err := jwt.Parse(request.Access, func(token *jwt.Token) (interface{}, error) {
			if token.Method.Alg() != s.AccessTokenConfig.SigningMethod {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return s.AccessTokenConfig.SigningKey, nil
		})

		if err != nil {
			return c.JSON(http.StatusBadRequest, ErrInvalidToken.Localize(lang, s.translator))
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			username = claims["username"].(string)
		} else {
			return c.JSON(http.StatusBadRequest, ErrInvalidToken.Localize(lang, s.translator))
		}
		break
	case request.Login != "" && (request.Password != "" || request.GoogleId != ""):
		// Work with both email and username
		query := bson.M{
			"$or": []interface{}{
				bson.M{"username": request.Login},
				bson.M{"email": request.Login},
			},
		}
		if request.Password != "" {
			query["password"] = base64.StdEncoding.EncodeToString([]byte(request.Password))
		} else {
			query["googleId"] = request.GoogleId
		}
		// query := bson.M{
		// 	"password": base64.StdEncoding.EncodeToString([]byte(request.Password)),
		// 	"$or": []interface{}{
		// 		bson.M{"username": request.Login},
		// 		bson.M{"email": request.Login},
		// 	},
		// }
		resp := new(User)
		if err := s.DB.Collections["users"].Get(query, bson.M{"username": 1}, resp); err != nil {
			if isNotFound(err) {
				if err := s.DB.Collections["users"].Get(bson.M{"$or": []interface{}{
					bson.M{"username": request.Login},
					bson.M{"email": request.Login},
				}}, bson.M{"_id": 1}, nil); err != nil {
					if isNotFound(err) {
						return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
					} else {
						return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
					}
				}
				return c.JSON(http.StatusBadRequest, ErrInvalidPassword.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		} else {
			username = resp.Username
		}
		break
	default:
		return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
	}

	newToken, err := s.CreateAccessToken(username)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Can also return refresh token if right fields are provided
	if request.AppId != "" && request.Device != "" && request.PushId != "" && refreshToken == "" {
		query := bson.M{
			"app":      request.AppId,
			"device":   request.Device,
			"username": username,
		}

		// Check if it's a registred app
		if err := s.DB.Collections["apps"].Get(bson.M{"secret": request.AppId}, nil, nil); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrInvalidAppId.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}

		token, err := s.CreateRefreshToken(username, request.AppId, request.Device, request.PushId, true)
		if err != nil {
			log.Println(err)
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
		refreshToken = token.Token

		if err := s.DB.Collections["tokens"].Update(query, bson.M{"$set": token}, true); err != nil {
			log.Println(err)
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}

	}

	return c.JSON(http.StatusCreated, ResponseToken{AccessToken: newToken, RefreshToken: refreshToken})
}

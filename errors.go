package server

import "bitbucket.org/seka7/web/localization"

var (
	ErrNoTrip                 = &Error{Code: 1, MsgCode: "errNoTrip"}
	ErrNoPost                 = &Error{Code: 2, MsgCode: "errNoPost"}
	ErrNoUser                 = &Error{Code: 3, MsgCode: "errNoUser"}
	ErrBadRequest             = &Error{Code: 4, MsgCode: "errBadRequest"}
	ErrBadRequestWithError    = &Error{Code: 4, MsgCode: "errBadRequestWithError"}
	ErrInBlacklist            = &Error{Code: 5, MsgCode: "errInBlacklist"}
	ErrNotInBlacklist         = &Error{Code: 6, MsgCode: "errNotInBlacklist"}
	ErrExpiredToken           = &Error{Code: 7, MsgCode: "errExpiredToken"}
	ErrInvalidToken           = &Error{Code: 8, MsgCode: "errInvalidToken"}
	ErrInvalidContent         = &Error{Code: 9, MsgCode: "errInvalidContent"}
	ErrForbidden              = &Error{Code: 10, MsgCode: "errForbidden"}
	ErrInvalidAppId           = &Error{Code: 11, MsgCode: "errInvalidAppId"}
	ErrInvalidPassword        = &Error{Code: 12, MsgCode: "errInvalidPassword"}
	ErrInvalidJwtToken        = &Error{Code: 13, MsgCode: "errInvalidJwtToken"}
	ErrInternal               = &Error{Code: 14, MsgCode: "errInternal"}
	ErrInternalWithError      = &Error{Code: 14, MsgCode: "errInternalWithError"}
	ErrLikedTrip              = &Error{Code: 15, MsgCode: "errLikedTrip"}
	ErrNotLikedTrip           = &Error{Code: 16, MsgCode: "errNotLikedTrip"}
	ErrUsernameReseved        = &Error{Code: 17, MsgCode: "errUsernameReserved"}
	ErrUsernameTaken          = &Error{Code: 18, MsgCode: "errUsernameTaken"}
	ErrEmailTaken             = &Error{Code: 19, MsgCode: "errEmailTaken"}
	ErrOldAndNewSamePasswords = &Error{Code: 20, MsgCode: "errOldAndNewSamePasswords"}
	ErrFollowing              = &Error{Code: 21, MsgCode: "errFollowing"}
	ErrNotFollowing           = &Error{Code: 22, MsgCode: "errNotFollowing"}
	ErrFollowingBlacklist     = &Error{Code: 23, MsgCode: "errFollowingBlacklist"}
	ErrEmailTakenInQueue      = &Error{Code: 24, MsgCode: "errEmailTakenInQueue"}
	ErrNoComment              = &Error{Code: 25, MsgCode: "errNoComment"}
	ErrNotInRegistrationQueue = &Error{Code: 26, MsgCode: "errNotInRegistrationQueue"}
)

// const (
// 	ErrExpiredToken = iota + 1
// 	ErrInvalidToken
// 	ErrInvalidEmail
// 	ErrInvalidNickname
// 	ErrUserNotExists
// 	ErrInternal
// 	ErrInvalidContent
// 	ErrEmptyFields
// 	ErrForbidden
// 	ErrInvalidQuery
// 	ErrBadRequest
// 	ErrMethodNotAllowed
// 	ErrDenied
// 	ErrTripNotExists
// )

type Error struct {
	Code    uint32 `json:"code"`    // inner error code
	Message string `json:"message"` // message explaining error
	MsgCode string `json:"-"`
}

func (this *Error) Localize(language string, t localization.TInterface, data ...interface{}) *Error {
	if msg, err := t.T(this.MsgCode, language, false, data); err == nil {
		this.Message = msg
	} else {
		if language != "en" {
			return this.Localize("en", t, data)
		}
	}

	return this
}

type AuthError struct {
	ErrorStatus int
	Error       string
}

type CodeError struct {
	Error struct {
		Message string `json:"message"`
	} `json:"error"`
}

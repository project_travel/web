package server

import (
	"fmt"
	"regexp"

	"bitbucket.org/seka7/web/localization"
	"gopkg.in/mgo.v2/bson"
)

type Trip struct {
	Id          bson.ObjectId `json:"id,omitempty" bson:"_id"`
	Name        string        `json:"name,omitempty" bson:"name"`
	Description string        `json:"description" bson:"description"`
	Owners      []string      `json:"owners" bson:"owners"`
	Start       *Time         `json:"start" bson:"start"`
	End         *Time         `json:"end" bson:"end"`
	// Countries   []*PlaceObject         `json:"-" bson:"countries"`
	Cities    []*City                `json:"-" bson:"cities"`
	Hashtags  []string               `json:"hashtags,omitempty" bson:"hashtags"`
	Tags      map[string]interface{} `json:"tags,omitempty" bson:"tags"`
	Avatar    []*Photo               `json:"avatar,omitempty" bson:"avatar"`
	Visible   *bool                  `json:"visible,omitempty" bson:"visible"`
	Likes     *int                   `json:"likes,omitempty" bson:"likes"`
	Modified  *Time                  `json:"modified" bson:"modified"`
	HasLiked  *bool                  `json:"hasLiked,omitempty" bson:"hasLiked,omitempty"`
	Comments  int                    `json:"comments" bson:"comments"`
	NotShowTo []string               `json:"-" bson:"notShowTo"`

	DisplayCountries []*PlaceObject `json:"countries" bson:"-"`
	DisplayCities    []string       `json:"cities" bson:"-"`
	AdRequest        string         `json:"ad" bson:"-"`
}

func hasLiked(username string) bson.M {
	return bson.M{
		"$cond": []interface{}{
			bson.M{
				"$eq": []interface{}{
					bson.M{
						"$size": bson.M{
							"$setIntersection": []interface{}{
								[]interface{}{username},
								"$likes.username",
							},
						},
					}, 1,
				},
			},
			true,
			false,
		},
	}
}

type UpdateTrip struct {
	Modified    *Time   `json:"modified,omitempty" bson:"modified,omitempty"`
	Name        *string `json:"name,omitempty" bson:"name,omitempty"`
	Description *string `json:"description,omitempty" bson:"description,omitempty"`
	Visible     *bool   `json:"visible,omitempty" bson:"visible,omitempty"`
}

func (trip *Trip) Display(host string, L localization.TInterface) *Trip {
	trip.DisplayCountries = make([]*PlaceObject, 0)
	trip.DisplayCities = make([]string, len(trip.Cities))

	countries := make([]string, 0)
	cities := make([]string, len(trip.Cities))

	for i, v := range trip.Cities {
		add := true
		for _, country := range trip.DisplayCountries {
			if country.Id == v.CountryId {
				add = false
				break
			}
		}
		if add {
			trip.DisplayCountries = append(trip.DisplayCountries, &PlaceObject{Id: v.CountryId, Value: v.CountryName})
			countries = append(countries, v.CountryId)
		}

		trip.DisplayCities[i] = stripCityName(v.Name)
		cities[i], _ = L.T(v.Id, "en", false)
	}

	trip.AdRequest = fmt.Sprintf("%s/trips/%s/ads", host, trip.Id.Hex())
	return trip
}

// Translate translates country and city to provided language
func (trip *Trip) Translate(language string, f localization.TInterface) {
	for i, v := range trip.Cities {
		// Translate city
		trip.Cities[i].Name, _ = f.T(v.Id, language, false)
		trip.Cities[i].CountryName, _ = f.T(v.CountryId, language, false)
	}
	return
}

func stripCityName(city string) string {
	return regexp.MustCompile("[,\\(\\)_;:*]").Split(city, 2)[0]
}

package main

import (
	"flag"
	"log"

	server "bitbucket.org/seka7/web"
)

func main() {
	config := flag.String("config", "config.json", "configuration filename")
	isDev := flag.Bool("dev", false, "for local development")
	flag.Parse()

	s, err := server.CreateServer(*config, *isDev)
	if err != nil {
		log.Fatal(err)
	}
	// регистрируем и запускаем сервисы
	if err := s.Run(); err != nil {
		log.Fatal(err)
	}
}

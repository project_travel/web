package server

import (
	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type BlacklistRequest struct {
	Username string        `json:"username"`
	Trip     bson.ObjectId `json:"trip"`
	Post     bson.ObjectId `json:"post"`
}

func (s *Server) addToBlacklist(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	request := new(BlacklistRequest)
	if err := c.Bind(request); err != nil {
		c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	// Parse request
	switch {
	case request.Username != "" && request.Username != username:

		s.logger.WithFields(logrus.Fields{
			"username": username,
			"banned":   request.Username,
		}).Info("Blacklist")

		// Check if user exists
		if err := s.DB.Collections["users"].Get(bson.M{"username": request.Username}, nil, nil); err != nil {
			return s.handleUserError(c, err, request.Username, ErrNoUser.Localize(lang, s.translator))
		}

		// Add to blacklist
		if err := s.DB.Collections["users"].Update(
			bson.M{"username": username, "blacklist": bson.M{"$ne": request.Username}},
			bson.M{
				"$push": bson.M{"blacklist": request.Username},
				"$pull": bson.M{"following": request.Username},
			},
		); err != nil {
			return s.handleUserError(c, err, username, ErrInBlacklist.Localize(lang, s.translator))
		}

		if err := s.DB.Collections["users"].Update(
			bson.M{"username": request.Username, "blacklistedBy": bson.M{"$ne": username}},
			bson.M{
				"$push": bson.M{"blacklistedBy": username},
				"$pull": bson.M{"followers": username},
			},
		); err != nil {
			return s.handleUserError(c, err, username, ErrInBlacklist.Localize(lang, s.translator))
		}

		// Mark blacklisted user's content as not viewable
		go s.DB.Collections["posts"].UpdateAll(
			bson.M{"owner": request.Username, "notShowTo": bson.M{"$ne": username}},
			bson.M{"$push": bson.M{"notShowTo": username}},
		)

		go s.DB.Collections["trips"].UpdateAll(
			bson.M{"owners": request.Username, "notShowTo": bson.M{"$ne": username}},
			bson.M{"$push": bson.M{"notShowTo": username}},
		)

		break
	case request.Trip != bson.ObjectId(""):
		if err := s.DB.Collections["trips"].Get(bson.M{
			"_id":    request.Trip,
			"owners": bson.M{"$ne": username},
		}, bson.M{"_id": 1}, nil); err != nil {
			return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
		}

		if err := s.DB.Collections["trips"].UpdateAll(
			bson.M{
				"_id":       request.Trip,
				"notShowTo": bson.M{"$ne": username},
			},
			bson.M{"$push": bson.M{"notShowTo": username}},
		); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrInBlacklist.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}

		go s.DB.Collections["trips"].UpdateAll(
			bson.M{"tripId": request.Trip, "notShowTo": bson.M{"$ne": username}},
			bson.M{"$push": bson.M{"notShowTo": username}},
		)

		break
	case request.Post != bson.ObjectId(""):
		if err := s.DB.Collections["posts"].Get(bson.M{
			"_id":   request.Post,
			"owner": bson.M{"$ne": username},
		}, bson.M{"_id": 1}, nil); err != nil {
			return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
		}

		if err := s.DB.Collections["posts"].Update(
			bson.M{"_id": request.Post, "notShowTo": bson.M{"$ne": username}},
			bson.M{"$push": bson.M{"notShowTo": username}},
		); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrInBlacklist.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}

		break
	default:
		return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) removeFromBlacklist(c echo.Context) error {
	username := getUsernameFromToken(c)
	lang := getLanguage(c, s.translator)

	request := new(BlacklistRequest)
	if err := c.Bind(request); err != nil {
		c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	// Parse request
	switch {
	case request.Username != "" && request.Username != username:
		s.logger.WithFields(logrus.Fields{
			"username": username,
			"unbanned": request.Username,
		}).Info("Blacklist")

		// Remove from blacklist
		if err := s.DB.Collections["users"].Update(
			bson.M{"username": username, "blacklist": request.Username},
			bson.M{"$pull": bson.M{"blacklist": request.Username}},
		); err != nil {
			return s.handleUserError(c, err, username, ErrNotInBlacklist.Localize(lang, s.translator))
		}

		if err := s.DB.Collections["users"].Update(
			bson.M{"username": request.Username, "blacklistedBy": username},
			bson.M{"$pull": bson.M{"blacklistedBy": username}},
		); err != nil {
			return s.handleUserError(c, err, username, ErrNotInBlacklist.Localize(lang, s.translator))
		}

		// Mark blacklisted user's content as viewable
		go s.DB.Collections["posts"].UpdateAll(
			bson.M{"owner": request.Username, "notShowTo": username},
			bson.M{"$pull": bson.M{"notShowTo": username}},
		)

		go s.DB.Collections["trips"].UpdateAll(
			bson.M{"owners": request.Username, "notShowTo": username},
			bson.M{"$pull": bson.M{"notShowTo": username}},
		)

		break
	case request.Trip != bson.ObjectId(""):
		if err := s.DB.Collections["trips"].UpdateAll(
			bson.M{
				"_id":       request.Trip,
				"notShowTo": username,
			},
			bson.M{"$pull": bson.M{"notShowTo": username}},
		); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrNotInBlacklist.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}

		go s.DB.Collections["posts"].UpdateAll(
			bson.M{"tripId": request.Trip, "notShowTo": username},
			bson.M{"$pull": bson.M{"notShowTo": username}},
		)

		break
	case request.Post != bson.ObjectId(""):
		if err := s.DB.Collections["posts"].Update(
			bson.M{"_id": request.Post, "notShowTo": username},
			bson.M{"$pull": bson.M{"notShowTo": username}},
		); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrNotInBlacklist.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}

		break
	default:
		return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) getBlacklistedUsers(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err))
	}

	users := make([]*User, 0)
	search := formUserSearchQuery(c.QueryParam("q"))
	search["blacklistedBy"] = username
	if err := s.DB.Collections["users"].AggregateAll([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$project": userProjection(getUsernameFromToken(c)),
		},
		bson.M{
			"$sort": formSortQuery("lastname,firstname,username"),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &users); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, users)
}

func (s *Server) getBlacklistedTrips(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	trips := make([]*Trip, 0)
	if err := s.DB.Collections["trips"].GetAll(
		bson.M{"notShowTo": username},
		nil,
		offset,
		size,
		"",
		&trips,
	); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, trips)
}

func (s *Server) getBlacklistedPosts(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	offset, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	posts := make([]*Post, 0)
	if err := s.DB.Collections["posts"].GetAll(
		bson.M{"notShowTo": username},
		nil,
		offset,
		size,
		"",
		&posts,
	); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, posts)
}

package skyscanner

import (
	"fmt"
	"time"

	"github.com/labstack/echo"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
)

const timeFormat = "2006-01-02"

type Ad struct {
	key    string `json:"key"`
	source string
}

type GetResponse struct {
	Source    string `json:"source"`
	City      string `json:"city"`
	CityId    string `json:"-"`
	Country   string `json:"country"`
	CountryId string `json:"-"`
	Origin    bool   `json:"origin"`
	Link      string `json:"url"`
}

func (this GetResponse) GetCity() string {
	return this.City
}

func (this GetResponse) GetCountry() string {
	return this.Country
}

func (this GetResponse) GetCityId() string {
	return this.CityId
}

func (this GetResponse) GetCountryId() string {
	return this.CountryId
}

func (this *GetResponse) SetCountry(country string) {
	this.Country = country
}

func (this *GetResponse) SetCountryId(id string) {
	this.CountryId = id
}

func (this *GetResponse) SetCity(city string) {
	this.City = city
}

func (this *GetResponse) SetCityId(id string) {
	this.CityId = id
}

type Geo interface {
	GetPoint() []float64
	GetCity() string
	GetCountry() string
	GetCityId() string
	GetCountryId() string
}

func Init(key string) *Ad {
	return &Ad{key: key, source: "skyscanner"}
}

// http://partners.api.skyscanner.net/apiservices/referral/v1.0/ru/GBP/en-GB/55.9,-3.18-latlong/CDG/2016-09-18/2016-09-25/?apiKey=prtl674938798674
// countryCode/currency/locale/from/to/when_in/when_out?key
// Get gets list of links to given cities.
func (this *Ad) Get(ppoints interface{}, c echo.Context, departure, arriaval *time.Time) []*GetResponse {
	points := make([]Geo, 0)
	for _, p := range ppoints.([]interface{}) {
		if v, ok := p.(Geo); ok {
			points = append(points, v)
		}
	}
	response := make([]*GetResponse, len(points))
	// IP address of request, pass it as starting place
	from := c.RealIP()
	lang, _, err := language.ParseAcceptLanguage(c.Request().Header.Get("Accept-Language"))
	// Can't get language from header, use english
	if err != nil {
		lang = []language.Tag{language.English}
	}

	var market language.Region
	for _, l := range lang {
		market, _ = l.Region()
		if market.IsCountry() {
			break
		}
	}

	// lang[0] is the most upropriate language
	// Get currency from language
	cur, _ := currency.FromTag(lang[0])

	var depTime, arTime string
	if departure == nil {
		depTime = time.Now().UTC().Format(timeFormat)
	} else {
		depTime = departure.Format(timeFormat)
	}

	if arriaval != nil {
		arTime = arriaval.Format(timeFormat)
	}

	for i, point := range points {
		coord := point.GetPoint() // coordinates of the city
		response[i] = &GetResponse{
			Source:    this.source,
			City:      point.GetCity(),
			CityId:    point.GetCityId(),
			Country:   point.GetCountry(),
			CountryId: point.GetCountryId(),
			Origin:    true,
			Link: fmt.Sprintf("http://partners.api.skyscanner.net/apiservices/referral/v1.0/%s/%s/%s/%s-ip/%f,%f-latlong/%s/%s?apiKey=%s",
				market, cur, lang[0], from, coord[0], coord[1], depTime, arTime, this.key),
		}
	}

	return response
}

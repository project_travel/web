package server

import (
	"bytes"
	"fmt"
	"html/template"

	gomail "gopkg.in/gomail.v2"
)

type MailClient struct {
	Name      string `json:"name"`
	Support   string `json:"support"` // mail address of support
	Password  string `json:"password"`
	Smtp      string `json:"smtp"`
	Port      int    `json:"port"`
	Dirname   string `json:"templates"`
	templates map[string]*template.Template
	dialer    *gomail.Dialer
	server    *Server
	// Paths     map[string]string `json:"paths"`
}

type VerificationLetter struct {
	Name                   string
	Link                   string
	PrivacyPolicyLink      string
	TermsAndConditionsLink string
	HomePageLink           string
}

func (this *MailClient) Init(server *Server) (err error) {
	if this.Support == "" {
		this.Support = "support@inspiry.me"
	}
	this.server = server
	this.dialer = gomail.NewDialer(this.Smtp, this.Port, this.Name, this.Password)
	this.templates = make(map[string]*template.Template)
	for _, lang := range server.translator.Languages() {
		this.templates[lang], err = template.ParseGlob(fmt.Sprintf("%s/%s/*", this.Dirname, lang))
		if err != nil {
			return
		}
	}
	return
}

func (this *MailClient) SendToSupport(email, msg string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", this.Name)
	m.SetHeader("To", this.Support)
	m.SetHeader("Subject", "From form")

	m.SetBody("text/html", fmt.Sprintf(`<p>Email address: %s<p><p>Message body: %s<p>`, email, msg))
	return this.dialer.DialAndSend(m)
}

func (this *MailClient) SendVerification(address, name, code, lang string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", this.Name)
	m.SetHeader("To", address)
	m.SetHeader("Subject", "Verification")
	buf := new(bytes.Buffer)

	link := fmt.Sprintf("%s/users/%s/verify/%s", this.server.GetPath("api"), name, code)

	if err := this.getTemplate(lang).ExecuteTemplate(buf, "activateAccount.html", VerificationLetter{
		name,
		link,
		this.server.GetPath("privacy", lang),
		this.server.GetPath("terms", lang),
		this.server.GetPath("home", lang),
	}); err != nil {
		return err
	}

	m.SetBody("text/html", buf.String())
	return this.dialer.DialAndSend(m)
}

func (this *MailClient) SendPassword(address, name, password, lang string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", this.Name)
	m.SetHeader("To", address)
	m.SetHeader("Subject", "Password reminder")
	buf := new(bytes.Buffer)

	if err := this.getTemplate(lang).ExecuteTemplate(buf, "forgotPassword.html", struct {
		Name                   string
		Password               string
		PrivacyPolicyLink      string
		TermsAndConditionsLink string
		HomePageLink           string
	}{
		name,
		password,
		this.server.GetPath("privacy", lang),
		this.server.GetPath("terms", lang),
		this.server.GetPath("home", lang),
	}); err != nil {
		return err
	}

	m.SetBody("text/html", buf.String())
	return this.dialer.DialAndSend(m)
}

func (this *MailClient) SendConfirmEmail(address, name, code, lang string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", this.Name)
	m.SetHeader("To", address)
	m.SetHeader("Subject", "Confirm email address")
	buf := new(bytes.Buffer)

	link := fmt.Sprintf("%s/users/%s/verify_email/%s", this.server.GetPath("api"), name, code)

	if err := this.getTemplate(lang).ExecuteTemplate(buf, "confirmEmail.html", VerificationLetter{
		name,
		link,
		this.server.GetPath("privacy", lang),
		this.server.GetPath("terms", lang),
		this.server.GetPath("home", lang),
	}); err != nil {
		return err
	}

	m.SetBody("text/html", buf.String())
	return this.dialer.DialAndSend(m)
}

func (this *MailClient) SendChangeEmailNotification(address, name, email, lang string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", this.Name)
	m.SetHeader("To", address)
	m.SetHeader("Subject", "Changing email notification")
	buf := new(bytes.Buffer)

	if err := this.getTemplate(lang).ExecuteTemplate(buf, "changeEmailNotification.html", struct {
		Name                   string
		Email                  string
		PrivacyPolicyLink      string
		TermsAndConditionsLink string
		HomePageLink           string
	}{
		name,
		email,
		this.server.GetPath("privacy", lang),
		this.server.GetPath("terms", lang),
		this.server.GetPath("home", lang),
	}); err != nil {
		return err
	}

	m.SetBody("text/html", buf.String())
	return this.dialer.DialAndSend(m)
}

func (this MailClient) getTemplate(lang string) *template.Template {
	if templ, ok := this.templates[lang]; ok {
		return templ
	} else {
		return this.templates["en"]
	}
}

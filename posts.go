package server

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/seka7/web/localization"

	"gopkg.in/mgo.v2/bson"
)

type Post struct {
	Id       bson.ObjectId          `bson:"_id,omitempty" json:"id"`
	Owner    string                 `json:"owner" bson:"owner"`
	Text     string                 `json:"text,omitempty" bson:"text,omitempty"`
	Photo    []*Photo               `json:"photo,omitempty" bson:"photo,omitempty"`
	Location *Geo                   `json:"location,omitempty" bson:"location,omitempty"` // exact location of post
	Place    *Place                 `json:"place,omitempty" bson:"place,omitempty"`
	Visible  bool                   `json:"visible" bson:"visible"`
	Source   *Source                `json:"source" bson:"source"`
	Created  *Time                  `json:"created" bson:"created"`
	Hashtags []string               `json:"hashtags" bson:"hashtags"`
	Tags     map[string]interface{} `json:"tags,omitempty" bson:"tags,omitempty"`
	Modified *Time                  `json:"modified" bson:"modified,omitempty"`
	TripId   bson.ObjectId          `json:"tripId,omitempty" bson:"tripId,omitempty"`

	NotShowTo []string `json:"-" bson:"notShowTo"`
}

type SavePost struct {
	Id       bson.ObjectId          `bson:"_id,omitempty" json:"id"`
	Owner    string                 `json:"owner" bson:"owner"`
	Text     string                 `json:"text,omitempty" bson:"text,omitempty"`
	Photo    []*Photo               `json:"photo,omitempty" bson:"photo,omitempty"`
	Location *Geo                   `json:"location,omitempty" bson:"location,omitempty"` // exact location of post
	Place    *Place                 `json:"place,omitempty" bson:"place,omitempty"`
	Visible  bool                   `json:"visible" bson:"visible"`
	Source   *Source                `json:"source" bson:"source"`
	Created  *Time                  `json:"created" bson:"created"`
	Hashtags []string               `json:"hashtags" bson:"hashtags"`
	Tags     map[string]interface{} `json:"tags,omitempty" bson:"tags,omitempty"`
	Modified *Time                  `json:"modified" bson:"modified,omitempty"`
	History  bool                   `json:"-" bson:"history"`
}

type Place struct {
	Location *Geo         `json:"location" bson:"location"`
	Country  *PlaceObject `json:"country,omitempty" bson:"country,omitempty"`
	City     *PlaceObject `json:"city,omitempty" bson:"city,omitempty"`
	Name     string       `json:"name,omitempty" bson:"name,omitempty"`
}

type PlaceObject struct {
	Id    string `json:"id" bson:"id"`
	Value string `json:"value" bson:"value"`
}

type City struct {
	Id          string `json:"id" bson:"id"`
	Name        string `json:"-" bson:"-"`
	CountryId   string `json:"countryId" bson:"countryId"`
	CountryName string `json:"-" bson:"-"`
	Location    *Geo   `json:"location" bson:"location"`
}

func (this City) GetCity() string {
	return this.Name
}

func (this City) GetCityId() string {
	return this.Id
}

func (this City) GetCountry() string {
	return this.CountryName
}

func (this City) GetCountryId() string {
	return this.CountryId
}

func (this City) GetPoint() []float64 {
	if this.Location != nil && this.Location.Type == "Point" {
		switch v := this.Location.Coordinates.(type) {
		case []float64:
			return v
		case []interface{}:
			tmp := make([]float64, 2)
			for i, p := range v {
				tmp[i] = p.(float64)
			}
			return tmp
		}

	}
	return []float64{0, 0}

}

type Source struct {
	Name     string `json:"name" bson:"name"`
	Id       string `json:"id,omitempty" bson:"id"` // id in original social network
	Platform string `json:"platform,omitempty" bson:"platform,omitempty"`
	Url      string `json:"url" bson:"url"`
	Likes    int    `json:"likes,omitempty" bson:"likes"`
	// Created  Time   `json:"created" bson:"created"`
}

type Photo struct {
	Width  int    `json:"width" bson:"width"`
	Height int    `json:"height" bson:"height"`
	Url    string `json:"url" bson:"url"`
	Type   string `json:"type,omitempty" bson:"type,omitempty"`
}

func toPosts(data interface{}, L localization.TInterface) []*SavePost {
	switch v := data.(type) {
	case []InstagramData:
		posts := make([]*SavePost, 0)
		for _, data := range v {
			// If it's not photo, then skip
			photos := make([]*Photo, 0)
			if data.Type != "image" {
				continue
			}

			for k, photo := range data.Photos {
				tmp := &Photo{
					Width:  photo.Width,
					Height: photo.Height,
					Url:    photo.Url,
				}
				switch k {
				case "thumbnail":
					tmp.Type = "thumb"
					break
				case "standard_resolution":
					tmp.Type = "original"
					break
				}
				photos = append(photos, tmp)
			}

			post := &SavePost{
				Hashtags: data.Hashtags,
				Created:  data.Created,
				Visible:  true,
				Photo:    photos,
				Modified: NewTime(time.Now()),
				Source: &Source{
					Name:  "instagram",
					Id:    data.Id,
					Url:   data.Link,
					Likes: data.Likes.Count,
				},
			}
			// Add text if exists
			if data.Text != nil {
				post.Text = data.Text.Text
			}

			// Add if location exists
			if data.Location != nil {
				post.Location = &Geo{
					Type:        "Point",
					Coordinates: []float64{data.Location.Longitude, data.Location.Latitude},
				}
				post.Place = &Place{
					Location: post.Location,
					Name:     data.Location.Name,
				}
			}
			posts = append(posts, post)
		}
		return posts
	case Tweet:
		// Ignore retweets and replies
		if v.Retweet || v.IsStatusReply != "" {
			return nil
		}

		s := 1
		var hasMedia bool
		var tags []string

		if v.Entities != nil {
			tags = make([]string, len(v.Entities.Tags))

			for i, tag := range v.Entities.Tags {
				tags[i] = tag.Text
			}

			hasMedia = v.Entities.Media != nil
			if hasMedia {
				s = len(v.Entities.Media)
			}
		}

		place := new(Place)
		if v.Place != nil {
			place.Location = v.Place.Geo.Correct()
			place.Country = &PlaceObject{
				Id:    v.Place.CountryCode,
				Value: v.Place.Country,
			}

			if v.Place.Type == "city" {
				code, err := L.T(v.Place.Name, "en", true)
				if err != nil {
					log.Println("Can't find code for city", v.Place.Name)
					code = ""
				}
				place.City = &PlaceObject{
					Id:    code,
					Value: v.Place.Name,
				}
			}
		} else {
			place = nil
		}

		posts := make([]*SavePost, s)
		for i := 0; i < s; i++ {
			txt := v.Text
			if hasMedia {
				txt = trimLink(txt)
			}
			posts[i] = &SavePost{
				Text:     txt,
				Location: v.Geo.Correct(),
				Place:    place,
				Visible:  true,
				Created:  v.Time,
				Hashtags: tags,
				Modified: NewTime(time.Now()),
				Source: &Source{
					Name: "twitter",
					Id:   v.Id,
					// Url:   "https://twitter.com/statuses/" + v.Id,
					Url:   v.Url,
					Likes: v.Likes,
				},
			}
		}

		if v.Entities.Media != nil {
			for i, m := range v.Entities.Media {
				if m.Type != "photo" {
					continue
				}
				thumb := &Photo{
					Width:  m.Sizes["thumb"].Width,
					Height: m.Sizes["thumb"].Height,
					Type:   "thumb",
					Url:    fmt.Sprintf("%s:%s", m.Url, "thumb"),
				}
				medium := &Photo{
					Width:  m.Sizes["medium"].Width,
					Height: m.Sizes["medium"].Height,
					Type:   "original",
					Url:    fmt.Sprintf("%s:%s", m.Url, "medium"),
				}
				small := &Photo{
					Width:  m.Sizes["small"].Width,
					Height: m.Sizes["small"].Height,
					Url:    fmt.Sprintf("%s:%s", m.Url, "small"),
				}
				large := &Photo{
					Width:  m.Sizes["large"].Width,
					Height: m.Sizes["large"].Height,
					Url:    fmt.Sprintf("%s:%s", m.Url, "large"),
				}
				posts[i].Photo = []*Photo{medium, thumb, small, large}
			}
		}

		return posts
	}
	return nil
}

// tripLink removes link from messages like bla-bla-bla http://...
func trimLink(msg string) string {
	for i := len(msg); i != 0; i-- {
		if msg[i-1] == ' ' {
			return msg[:i-1]
		}
	}
	return msg
}

package server

import (
	"errors"
	"net/http"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type SocialMediaInfo struct {
	Token           string     `bson:"accessToken" json:"-"`
	Secret          string     `bson:"accessSecret" json:"-"`
	User            SocialUser `bson:"user" json:"-"`
	DisplayUsername string     `json:"username"`
}

type SocialUser struct {
	Id             string `bson:"id"`
	Username       string `bson:"username"`
	FullName       string `bson:"fullName"`
	ProfilePicture string `bson:"profilePicture"`
}

func (s *Server) getSocialMediaInfo(username, t string) (*SocialMediaInfo, error) {
	query := bson.M{"username": username}
	sel := bson.M{t: 1}
	resp := make(map[string]*SocialMediaInfo)
	if err := s.DB.Collections["users"].Get(query, sel, &resp); err != nil {
		return nil, err
	}
	info, ok := resp[t]
	if !ok {
		return nil, errors.New("not found")
	}
	return info, nil
}

func (s *Server) getSocialMediaList(username string) ([]string, error) {

	query := bson.M{"username": username}
	sel := bson.M{"_id": 0, "twitter": 1, "instagram": 1, "facebook": 1, "vk": 1}
	resp := make(map[string]interface{})
	if err := s.DB.Collections["users"].Get(query, sel, &resp); err != nil {
		return nil, err
	}
	var keys []string
	for k, v := range resp {
		if v != nil {
			keys = append(keys, k)
		}
	}
	return keys, nil
}

func (s *Server) getUserSocialMedias(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username, _ := getUsername(c)

	query := bson.M{"username": username}
	sel := bson.M{"_id": 0, "twitter.user.username": 1, "instagram.user.username": 1, "facebook.user.username": 1, "vk.user.username": 1}
	resp := make(map[string]*SocialMediaInfo)
	if err := s.DB.Collections["users"].Get(query, sel, &resp); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	for i, v := range resp {
		resp[i].DisplayUsername = v.User.Username
	}

	return c.JSON(http.StatusOK, resp)
}

func (s *Server) removeUserSocialMedias(c echo.Context) error {
	switch c.Param("name") {
	case "instagram":
		return s.deleteInstagram(c)
	case "twitter":
		return s.deleteTwitter(c)
	default:
		lang := getLanguage(c, s.translator)
		return c.JSON(http.StatusBadRequest, ErrBadRequest.Localize(lang, s.translator))
	}
}

func (s *Server) removeSocialMediaData(username string, name string) error {
	if err := s.DB.Collections["posts"].DeleteAll(
		bson.M{
			"owner":       username,
			"source.name": name,
		},
	); err != nil {
		return err
	}

	trips := make([]Trip, 0)

	if err := s.DB.Collections["trips"].GetAll(bson.M{
		"owners": username,
	}, bson.M{
		"_id": 1,
	}, 0, 0, "", &trips); err != nil {
		return err
	}

	for _, trip := range trips {
		posts := make([]Post, 0)
		if err := s.DB.Collections["posts"].GetAll(bson.M{
			"tripId": trip.Id,
		}, bson.M{
			"_id": 1,
		}, 0, 0, "", &posts); err != nil {
			return err
		}
		if len(posts) == 0 {
			if err := s.DB.Collections["trips"].Delete(
				bson.M{
					"_id": trip.Id,
				},
			); err != nil {
				return err
			}
		}
	}

	return nil

}

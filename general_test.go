package server

import (
	"fmt"
	"log"
	"testing"
	"time"

	"gopkg.in/mgo.v2/bson"
)

func TestAlgorithmsLarge(t *testing.T) {
	server, err := CreateServer("./main/test.json", false)
	if err != nil {
		t.FailNow()
	}

	go func(server *Server) {
		if err := server.Run(); err != nil {
			fmt.Println(err)
			t.Error(err)
			// t.FailNow()
		}
	}(server)

	fmt.Println("Waiting server to start")
	time.Sleep(time.Second * 3)
	fmt.Println("Stor waiting")

	server.Rabbit.FormTrips(TripsUpdate{
		Id:       "12345",
		Start:    nil,
		End:      nil,
		Username: "large",
		Update:   false,
	})

	for {
	}

}

func TestAlgorithmsMany(t *testing.T) {
	server, err := CreateServer("./main/test.json", false)
	if err != nil {
		t.FailNow()
	}

	go func(server *Server) {
		if err := server.Run(); err != nil {
			fmt.Println(err)
			t.Error(err)
			// t.FailNow()
		}
	}(server)

	fmt.Println("Waiting server to start")
	time.Sleep(time.Second * 4)
	fmt.Println("Stor waiting")

	for i := 0; i < 10; i++ {
		// time.Sleep(time.Second * 4)
		if err := server.Rabbit.FormTrips(TripsUpdate{
			Id:       fmt.Sprintf("id_test%v", i),
			Start:    nil,
			End:      nil,
			Username: fmt.Sprintf("test%v", i),
			Update:   false,
		}); err != nil {
			log.Println(err)
		}
	}

	for {
	}

}

func TestMakeDuplicates(t *testing.T) {

	server, err := CreateServer("./main/test.json", true)
	if err != nil {
		t.FailNow()
	}

	go func(server *Server) {
		if err := server.Run(); err != nil {
			fmt.Println(err)
			t.Error(err)
			// t.FailNow()
		}
	}(server)

	fmt.Println("Waiting server to start")
	time.Sleep(time.Second * 2)
	fmt.Println("Stor waiting")

	posts := make([]*SavePost, 0)
	server.DB.Collections["posts"].GetAll(bson.M{"owner": "testme"}, bson.M{"_id": 0}, 0, 0, "", &posts)
	fmt.Println(len(posts))
	// var wg sync.WaitGroup
	// wg.Add(20)
	for i := 0; i < 20; i++ {
		// go func() {
		for z, _ := range posts {
			posts[z].Owner = fmt.Sprintf("test%v", i)
		}

		for j, v := range posts {
			// fmt.Println(v)
			if err := server.DB.Collections["posts"].Create(v); err != nil {
				fmt.Println(j, err)
			}
			// fmt.Println(j, nil)
		}
		// wg.Done()
		// }()
	}

	// wg.Wait()
}

func TestTMP(t *testing.T) {
	tt := bson.NewObjectId()
	tmp := BlacklistRequest{Post: tt}
	fmt.Println(tmp)
	if !(tmp.Post == tt) {
		t.FailNow()
	}
}

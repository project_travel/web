package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type Pusher struct {
	Id      string `json:"id"`
	Address string `json:"address"`
	Secret  string `json:"secret"`
	Server  *Server
}

var (
	pushWasLiked     = `Your trip "%s" was liked by %s`
	pushWasCommented = `Your trip "%s" was commented by %s`
	pushWasFollowed  = "%s starts to follow you"
)

type Push struct {
	App     string                 `json:"app_id"`             // secret app id
	Devices []string               `json:"include_player_ids"` // devices ids which will recive this push
	Data    map[string]interface{} `json:"data"`               // additional data
	Message map[string]string      `json:"contents"`           // {"en": "Hello!", "ru": "Привет!"}
}

// Send sends push notification to all username devices
// action -- additional data
// -------------------------------------------
// like -- trip id -- trip name -- username
// comment -- trip id -- trip name -- username
// follower -- username
// trips -- newIds -- updatedIds -- isUpdateFailed
func (this *Pusher) Send(username string, action string, additionalData ...interface{}) error {
	data := make(map[string]interface{})

	// Parse action
	switch action {
	case "like":
		data["type"] = "newLike"
		data["tripId"] = additionalData[0]
		data["tripName"] = additionalData[1]
		data["username"] = additionalData[2]
		break
	case "comment":
		data["type"] = "newComment"
		data["tripId"] = additionalData[0]
		data["tripName"] = additionalData[1]
		data["username"] = additionalData[2]
		break
	case "follower":
		data["type"] = "newFollower"
		data["followerUsername"] = additionalData[0]
		// dataForMessage["username"] = additionalData[0]
		break
	case "trips":
		data["type"] = "newTripsExtracted"
		data["newIds"] = additionalData[0].([]string)
		data["updatedIds"] = additionalData[1].([]string)
		data["isUpdateFailed"] = additionalData[2]
		if len(additionalData) == 4 {
			data["isUserError"] = additionalData[3]
		}
		break
	default:
		return fmt.Errorf("Unknown action")
	}

	go this.saveNotification(username, data)

	// Get devices associated with the username
	tmp := make([]bson.M, 0)
	if err := this.Server.DB.Collections["tokens"].GetAll(
		bson.M{"username": username},
		bson.M{"_id": 0, "push": 1},
		0,
		0,
		"",
		&tmp,
	); err != nil {
		return err
	}

	hasSomethingToPush := false
	devices := make([]string, len(tmp))
	for i, v := range tmp {
		pushId, ok := v["push"].(string)
		if !ok {
			continue
		}
		devices[i] = pushId
		hasSomethingToPush = true
	}

	// No users registred for push messages
	if !hasSomethingToPush {
		return nil
	}

	message, err := createNotificationMessage(data, this.Server.translator)
	if err != nil {
		return err
	}

	if message == nil {
		return errors.New("Empty message body")
	}

	this.Server.logger.WithField("push", Push{this.Id, devices, data, message}).Info("Sending push")

	return this.send(Push{this.Id, devices, data, message})
}

func (this *Pusher) send(push Push) error {
	// fmt.Printf("Sending push -> %#v\n", push)
	buf := bufPool.Get().(*bytes.Buffer)
	buf.Reset()
	defer bufPool.Put(buf)

	if b, err := json.Marshal(push); err != nil {
		return err
	} else {
		if _, err := buf.Write(b); err != nil {
			return err
		}
	}
	client := http.Client{}
	req, err := http.NewRequest("POST", this.Address, buf)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", "Basic "+this.Secret)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")

	response, err := client.Do(req)
	// response, err := http.Post(this.Address, "application/json", buf)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		buf.Reset()
		if _, err := buf.ReadFrom(response.Body); err != nil {
			return err
		}
		return fmt.Errorf("Got code %d, expected 200. Message: %s", response.StatusCode, buf.String())
	}
	buf.Reset()

	if _, err := buf.ReadFrom(response.Body); err != nil {
		return err
	}

	answer := make(map[string]interface{})
	if err := json.Unmarshal(buf.Bytes(), &answer); err != nil {
		return err
	}

	if errs, ok := answer["errors"]; ok {
		// Handle error
		switch v := errs.(type) {
		// Returned if using include_player_ids and some were valid and others were not.
		case map[string]interface{}:
			invalidDevices, ok := v["invalid_player_ids"].([]interface{})
			if !ok {
				return errors.New("Can't parse error response")
			}

			// Delete devices, which aren't registred in push service
			go this.Server.DB.Collections["tokens"].DeleteAll(bson.M{"push": bson.M{"$in": invalidDevices}})
			// Check, that devices recived push message
			return nil
		// Signals, that no one subscribed to push notification
		case []string:
			return nil
		default:
			return errors.New("Unsupported structure of errors object")
		}
	}

	// Everything is fine
	return nil
}

// For testing purposes
func (this *Pusher) TestPush(push Push) error {
	return this.send(push)
}

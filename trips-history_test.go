package server

import (
	"testing"
	"time"

	"github.com/Sirupsen/logrus"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestHistoryCrud(t *testing.T) {
	server := &Server{
		MongoAddress: "mongodb://localhost:27017/test",
	}

	server.DB, _ = server.InitDB(server.MongoAddress, logrus.StandardLogger())

	if err := server.DB.AddCollection("requestedTrips", []mgo.Index{mgo.Index{
		Key:         []string{"created"},
		ExpireAfter: time.Hour,
	}}); err != nil {
		t.Fatal(err)
	}

	id := bson.NewObjectId()
	trip := &Trip{Id: id}

	server.saveTripsHistory("testing", []*Trip{trip})

	ids := server.getTripsHistory("testing")

	found := false
	for _, i := range ids {
		if i.Hex() == id.Hex() {
			found = true
			break
		}
	}

	if !found {
		t.Fatal("Not found")
	}

	// Clear database

	server.removeTripsHistory("testing")

	if len(server.getTripsHistory("testing")) != 0 {
		t.Fatal("Not cleared")
	}

}

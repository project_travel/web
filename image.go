package server

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"path"
	"strings"
	"sync"

	"github.com/nfnt/resize"
)

var bufPool = &sync.Pool{
	New: func() interface{} { return &bytes.Buffer{} },
}

var possibleExtensions []string = []string{".jpg", ".jpeg", ".png"}

func imgToUint(img image.Image) []uint {
	return []uint{uint(img.Bounds().Size().X), uint(img.Bounds().Size().Y)}
}

func isResizable(in string) error {
	p := path.Ext(in)
	for _, v := range possibleExtensions {
		if v == p {
			return nil
		}
	}
	return fmt.Errorf("Only supports %s\n extensions", strings.Join(possibleExtensions, ", "))
}

// imageSize returns image size
func imageSize(data []byte, ext string) ([]uint, error) {
	buf := (bufPool.Get()).(*bytes.Buffer)
	buf.Reset()
	defer bufPool.Put(buf)
	if _, err := buf.Write(data); err != nil {
		return nil, err
	}

	var img image.Image
	var err error
	// Decode file into image.Image
	switch ext {
	case ".jpg", ".jpeg":
		img, err = jpeg.Decode(buf)
		if err != nil {
			return nil, err
		}
		break
	case ".png":
		img, err = png.Decode(buf)
		if err != nil {
			return nil, err
		}
		break
	default:
		// logger.WithField("error", errors.New("Unsupported extension")).Error("Can't get size of the image")
		return nil, errors.New("Unsupported extension")
	}

	return imgToUint(img), nil
}

// Resize creates copy of image with the provided sizes.
// If ratio parametr is set to true, then it saves aspect ratio.
func Resize(data []byte, extIn, extOut string, width, height uint, ratio bool) ([]byte, []uint, error) {
	buf := new(bytes.Buffer)
	if _, err := buf.Write(data); err != nil {
		return nil, nil, err
	}

	var img image.Image
	var err error
	// Decode file into image.Image
	switch extIn {
	case ".jpg", ".jpeg":
		img, err = jpeg.Decode(buf)
		if err != nil {
			return nil, nil, err
		}
		break
	case ".png":
		img, err = png.Decode(buf)
		if err != nil {
			return nil, nil, err
		}
		break
	default:
		// logger.WithField("error", errors.New("Unsupported extension")).Error("Can't resize picture")
		return nil, nil, errors.New("Unsupported extension")
	}

	// If save aspect ratio
	if ratio {
		height = 0
	}

	m := resize.Resize(width, height, img, resize.Lanczos3)

	buf.Reset()

	switch extOut {
	case ".jpg", ".jpeg":
		if err := jpeg.Encode(buf, m, &jpeg.Options{100}); err != nil {
			return nil, nil, err
		}
		break
	case ".png":
		if err := png.Encode(buf, m); err != nil {
			return nil, nil, err
		}
		break
	default:
		// logger.WithField("error", errors.New("Unsupported extension")).Error("Can't resize picture")
		return nil, nil, errors.New("Unsupported extension")
	}

	return buf.Bytes(), imgToUint(m), nil
}

package server

import (
	"bytes"
	"encoding/base64"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/seka7/web/localization"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func redirectAfterAuth(c echo.Context, authErr *AuthError, ru string) error {
	var redirectUrl string
	rUrl, err := base64.URLEncoding.DecodeString(ru)
	if err != nil || len(rUrl) == 0 {
		redirectUrl = "/"
	} else {
		redirectUrl = string(rUrl)
	}
	// log.Println("Url ->", redirectUrl)
	if authErr == nil {
		return c.Redirect(http.StatusFound, redirectUrl)
	}
	u, err := url.Parse(redirectUrl)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal)
	}
	q := u.Query()
	q.Set("error_status", strconv.Itoa(authErr.ErrorStatus))
	q.Set("error", authErr.Error)
	u.RawQuery = q.Encode()
	return c.Redirect(http.StatusFound, u.String())
}

func checkGender(g string) (string, error) {
	switch g {
	case "male", "m":
		return "male", nil
	case "female", "f":
		return "female", nil
	case "":
		return "", nil
	default:
		return "", errors.New("Unknown gender")
	}
}

func uniteOrders(order, defOrder string) string {
	tmp := strings.Join([]string{order, defOrder}, ",")
	for _, v := range strings.Split(defOrder, ",") {
		n := strings.Count(tmp, v)
		if n != 0 {
			tmp = strings.Replace(tmp, v+",", "", n)
		}
	}
	return tmp
}

func setPagination(c echo.Context, defOrder string) (offset int, size int, order string, err error) {
	offsetStr := c.QueryParam("offset")
	if offsetStr == "" {
		offsetStr = "0"
	}

	sizeStr := c.QueryParam("size")
	if sizeStr == "" {
		sizeStr = "25"
	}

	order = c.QueryParam("sort")
	if order == "" {
		order = defOrder
	} else {
		order = uniteOrders(order, defOrder)
	}

	var er error

	offset, er = strconv.Atoi(offsetStr)
	if er != nil {
		err = errors.New("from query must be integer")
		return
	}

	size, er = strconv.Atoi(sizeStr)
	if er != nil {
		err = errors.New("step query must be integer")
		return
	}

	if offset < 0 {
		err = errors.New("from query can't be negative")
		return
	}

	if size <= 0 {
		err = errors.New("step query can't be negative or equal to zero")
		return
	}

	return
}

func getId(c echo.Context, key string) (bson.ObjectId, error) {
	id := c.Param(key)
	if id == "" {
		return "", errors.New("Can't convert to ObjectId")
	}
	if bson.IsObjectIdHex(id) {
		return bson.ObjectIdHex(id), nil
	} else {
		return "", errors.New("Can't convert to ObjectId")
	}
}

// getUsername returns username from token, if provided. Otherwise it takes it from param.
// It also tells if request comming from user or not.
func getUsername(c echo.Context) (string, bool) {
	switch me := c.Get("me").(type) {
	case bool:
		if me {
			return getUsernameFromToken(c), true
		} else {
			if v, ok := c.Get("username").(string); ok {
				return v, false
			} else {
				return getUsernameFromToken(c), false
			}
		}
	}
	if c.Get("username") != nil {
		return c.Get("username").(string), false
	}
	return "", false
}

func getUsernameFromToken(c echo.Context) string {
	switch t := c.Get("access_token").(type) {
	case *jwt.Token:
		if t == nil {
			return ""
		}
		claims := t.Claims.(jwt.MapClaims)
		username, ok := claims["username"].(string)
		if ok {
			return username
		} else {
			return ""
		}
	default:
		return ""
	}
}

func getLanguageFromToken(c echo.Context) string {
	switch t := c.Get("access_token").(type) {
	case *jwt.Token:
		claims := t.Claims.(jwt.MapClaims)
		if l, ok := claims["lang"]; ok {
			if lang, ok := l.(string); ok {
				return lang
			} else {
				return ""
			}
		} else {
			return ""
		}
	default:
		return ""
	}
}

func getLanguage(c echo.Context, L *localization.Translations) string {
	tmp := getLanguageFromToken(c)
	if tmp == "" {
		return L.GetLanguageFromHeader(c)
	} else {
		return tmp
	}
}

func formSortQuery(str string) bson.D {
	res := bson.D{}
	tmp := strings.Split(str, ",")
	for _, item := range tmp {
		if item[0] == '-' {
			res = append(res, bson.DocElem{item[1:], -1})
		} else {
			res = append(res, bson.DocElem{item, 1})
		}
	}
	return res
}

func (s *Server) GetPath(args ...string) string {
	switch len(args) {
	case 1:
		return s.Paths["general"][args[0]]
	case 2:
		if p, ok := s.Paths[args[1]]; ok {
			return p[args[0]]
		} else {
			return s.Paths["en"][args[0]]
		}
	default:
		return ""
	}
}

func isNotFound(err error) bool {
	return err.Error() == mgo.ErrNotFound.Error()
}

func initPools() {
	for i := 0; i < 100; i++ {
		coordPool.Put([][]float64{})
		polygonPool.Put([][][]float64{})
		bufPool.Put(new(bytes.Buffer))
	}
}

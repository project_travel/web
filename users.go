package server

import (
	"log"
	"net/http"
	"regexp"
	"strings"

	"bitbucket.org/seka7/web/localization"

	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"

	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"-"`
	Email      string        `bson:"email,omitempty" json:"email,omitempty"`
	Username   string        `bson:"username,omitempty" json:"username,omitempty"`
	Password   string        `bson:"password,omitempty" json:"password,omitempty"`
	Surname    string        `bson:"surname,omitempty" json:"surname,omitempty"`
	Firstname  string        `bson:"firstname,omitempty" json:"firstname,omitempty"`
	Secondname string        `bson:"secondname,omitempty" json:"secondname,omitempty"`
	BirthDate  *Time         `bson:"birthDate,omitempty" json:"birthDate,omitempty"`
	Gender     string        `bson:"gender,omitempty" json:"gender,omitempty"`
	Status     string        `bson:"status,omitempty" json:"status,omitempty"`
	Avatar     []*Photo      `bson:"avatar,omitempty" json:"avatar,omitempty"`
	Background []*Photo      `bson:"background,omitempty" json:"background,omitempty"`
	Language   string        `bson:"language,omitempty" json:"language,omitempty"`

	AppId  string `bson:"-" json:"app,omitempty"`
	Device string `bson:"-" json:"device,omitempty"`
	PushId string `bson:"-" json:"push,omitempty"`

	GoogleId string `bson:"googleId,omitempty" json:"googleId,omitempty"`

	Following         []string `bson:"following" json:"-"`
	Followers         []string `bson:"followers" json:"-"`
	PossibleFollowing []string `bson:"possibleFollowing" json:"-"`
	Role              string   `bson:"role,omitempty" json:"role,omitempty"`
	RegistrationDate  *Time    `bson:"registrationDate,omitempty" json:"registrationDate,omitempty"`

	Blacklist     []string `bson:"blacklist" json:"-"`
	BlacklistedBy []string `bson:"blacklistedBy" json:"-"`

	InBlacklist     bool `bson:"inBlacklist" json:"inBlacklist"`
	HasFollowed     bool `bson:"hasFollowed" json:"hasFollowed"`
	LengthFollowing uint `bson:"lengthFollowing" json:"following"`
	LengthFollowers uint `bson:"lengthFollowers" json:"followers"`
}

type ChangePassword struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

type UpdateUser struct {
	// Email      string  `bson:"email,omitempty" json:"email,omitempty"`
	Surname    *string `bson:"surname,omitempty" json:"surname,omitempty"`
	Firstname  *string `bson:"firstname,omitempty" json:"firstname,omitempty"`
	Secondname *string `bson:"secondname,omitempty" json:"secondname,omitempty"`
	BirthDate  *Time   `bson:"birthDate,omitempty" json:"birthDate,omitempty"`
	Gender     *string `bson:"gender,omitempty" json:"gender,omitempty"`
	Status     *string `bson:"status,omitempty" json:"status,omitempty"`
	Language   *string `bson:"language,omitempty" json:"language,omitempty"`
}

func formUserSearchQuery(q string) bson.M {
	// All users
	search := bson.M{}
	if q != "" {
		tmp := strings.Split(q, " ")
		qq := make([]interface{}, len(tmp))
		for i, x := range tmp {
			qq[i] = bson.RegEx{"^" + x, "i"}
		}
		// Form search query
		search = bson.M{
			"$or": []interface{}{
				bson.M{"username": bson.M{"$in": qq}},
				bson.M{"lastname": bson.M{"$in": qq}},
				bson.M{"firstname": bson.M{"$in": qq}},
			},
		}
	}
	return search
}

func userProjection(username string) bson.M {
	return bson.M{
		"username":         1,
		"firstname":        1,
		"surname":          1,
		"secondname":       1,
		"birthDate":        1,
		"gender":           1,
		"status":           1,
		"avatar":           1,
		"background":       1,
		"role":             1,
		"registrationDate": 1,
		"lengthFollowers":  bson.M{"$size": "$followers"},
		"lengthFollowing":  bson.M{"$size": "$following"},
		"inBlacklist":      IfInArrayProjection(username, "blacklistedBy"),
		// "displayFollowers": bson.M{"$size": "$followers"},
		// "displayFollowing": bson.M{"$size": "$following"},
		"hasFollowed": IfInArrayProjection(username, "followers"),
	}
}

// Query searches user with given email or nickname.
// It also checks old nicknames.
func (user User) Query() interface{} {
	if user.Username == "" && user.Email == "" {
		return bson.M{"username": ""}
	}
	return bson.M{
		"$or": []interface{}{
			bson.M{"email": user.Email},
			bson.M{"username": user.Username},
		}}
}

// VerifyQuery do the same search as Query,
// but in addition it checks password
func (user User) VerifyQuery() interface{} {
	if user.Username == "" && user.Email == "" {
		return bson.M{"username": ""}
	}
	return bson.M{
		"$or": []interface{}{
			bson.M{"email": user.Email},
			bson.M{"username": user.Username},
		},
		"password": user.Password,
	}
}

func (s *Server) addPossibleFriends(username string) error {
	instFriends, twiFriends := s.GetPossibleFriends(username)
	s.logger.Println(instFriends, twiFriends)

	if twiFriends == nil && instFriends == nil {
		return nil
	}

	// Search them in database and return random friends
	search := bson.M{
		"$or":       []bson.M{},
		"followers": bson.M{"$ne": username},
	}

	if twiFriends != nil {
		search["$or"] = append(
			search["$or"].([]bson.M),
			bson.M{"twitter.user.id": bson.M{"$in": twiFriends}})
	}

	if instFriends != nil {
		search["$or"] = append(
			search["$or"].([]bson.M),
			bson.M{"instagram.user.id": bson.M{"$in": instFriends}})
	}

	possibleFollowing := make([]bson.M, 0)

	if err := s.DB.Collections["users"].GetAll(
		search,
		bson.M{"username": 1, "_id": 0},
		0, 0, "",
		&possibleFollowing,
	); err != nil {
		return err
	}

	tmp := make([]string, len(possibleFollowing))
	for i, v := range possibleFollowing {
		tmp[i] = v["username"].(string)
	}

	s.logger.WithFields(logrus.Fields{
		"username":          username,
		"possbileFollowing": tmp,
	}).Info("Possible following")

	if err := s.DB.Collections["users"].Update(
		bson.M{"username": username},
		bson.M{"$set": bson.M{"possibleFollowing": tmp}},
	); err != nil {
		return err
	}

	return nil
}

// handleUserError checks several options:
// 1) If error is "not found", then check that user exists, if he's not return "No such user" error,
// otherwise return provided error
// 2) If not, return internal error
func (s *Server) handleUserError(c echo.Context, err error, username string, outErr *Error) error {
	lang := getLanguage(c, s.translator)
	if isNotFound(err) {
		if err := s.DB.Collections["users"].Get(bson.M{"username": username}, bson.M{"_id": 1}, nil); err != nil {
			if isNotFound(err) {
				return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
			} else {
				return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
			}
		}
		return c.JSON(http.StatusBadRequest, outErr)
	} else {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
}

func (s *Server) GetPossibleFriends(username string) ([]string, []string) {
	instFriends, err := s.getInstagramFriends(username)
	if err != nil {
		log.Println("Can't get instagram friends ->", err)
	}
	twiFriends, err := s.getTwitterFriends(username)
	if err != nil {
		log.Println("Can't get twitter friends ->", err)
	}
	return instFriends, twiFriends
}

func isValidUsername(username string) bool {
	return regexp.MustCompile(`^[A-Za-z][A-Za-z0-9]{3,19}$`).MatchString(username)
}

func isValidEmail(email string) bool {
	return regexp.MustCompile(`@`).MatchString(email)
}

func isValidStatus(status string) bool {
	if len(status) > 140 || len(status) == 0 {
		return false
	} else {
		return true
	}
}

func isValidComment(comment string) bool {
	if len(comment) > 2000 || len(comment) == 0 {
		return false
	} else {
		return true
	}
}

func isValidDescription(description string) bool {
	if len(description) > 2000 || len(description) == 0 {
		return false
	} else {
		return true
	}
}

func isValidTripName(name string) bool {
	if len(name) > 100 || len(name) == 0 {
		return false
	} else {
		return true
	}
}

func isValidLanguage(lang string, L *localization.Translations) bool {
	for _, v := range L.Languages() {
		if v == lang {
			return true
		}
	}
	return false
}

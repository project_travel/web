package server

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

func (s *Server) getTripsHistory(username string) []bson.ObjectId {
	tmp := struct {
		Ids []bson.ObjectId `bson:"ids"`
	}{}
	s.DB.Collections["requestedTrips"].Get(bson.M{"_id": username}, bson.M{"ids": 1}, &tmp)

	return tmp.Ids
}

func (s *Server) saveTripsHistory(username string, trips []*Trip) {
	if len(trips) == 0 {
		return
	}

	ids := make([]bson.ObjectId, len(trips))

	for i, trip := range trips {
		ids[i] = trip.Id
	}

	s.DB.Collections["requestedTrips"].Update(bson.M{"_id": username}, bson.M{
		"$addToSet": bson.M{
			"ids": bson.M{"$each": ids},
		},
		// Update created time
		"$set": bson.M{
			"created": NewTime(time.Now().UTC()),
		}}, true)

	return
}

func (s *Server) removeTripsHistory(username string) {
	s.DB.Collections["requestedTrips"].Delete(bson.M{"_id": username})
}

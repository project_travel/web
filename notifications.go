package server

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/seka7/web/localization"

	"gopkg.in/mgo.v2/bson"

	"github.com/labstack/echo"
)

func (this *Pusher) saveNotification(username string, data map[string]interface{}) error {
	tmp := make(map[string]interface{})
	for i, v := range data {
		tmp[i] = v
	}
	tmp["created"] = NewTime(time.Now())
	tmp["owner"] = username
	return this.Server.DB.Collections["notifications"].Create(tmp)
}

func (s *Server) getNotifications(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username, _ := getUsername(c)
	if username == c.Param("username") {
		return c.JSON(http.StatusForbidden, ErrForbidden.Localize(lang, s.translator))
	}

	offset, size, order, er := setPagination(c, "-created,_id")
	if er != nil {
		return c.JSON(http.StatusBadRequest, er)
	}

	notifications := make([]bson.M, 0)

	if err := s.DB.Collections["notifications"].GetAll(
		bson.M{"owner": username},
		bson.M{"_id": 0, "owner": 0},
		offset,
		size,
		order,
		&notifications,
	); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Create message in user language
	for i := 0; i < len(notifications); i++ {
		message, err := createNotificationMessage(notifications[i], s.translator)
		if err != nil {
			s.logger.WithError(err).Error("Can't create notification message")
			continue
		}
		notifications[i]["message"] = message[lang]
		// Convert from standard time
		notifications[i]["created"] = NewTime(notifications[i]["created"].(time.Time))
	}

	return c.JSON(http.StatusOK, notifications)
}

func createNotificationMessage(data map[string]interface{}, L *localization.Translations) (map[string]string, error) {
	tmp, ok := data["type"]
	if !ok {
		return nil, errors.New("missing type field")
	}

	t := tmp.(string)
	// Parse action
	switch t {
	case "newLike", "newComment":
		tripName, ok := data["tripName"]
		if !ok {
			return nil, errors.New("missing tripName field")
		}
		username, ok := data["username"]
		if !ok {
			return nil, errors.New("missing username field")
		}
		return L.TAll(t, struct {
			Name     string
			Username string
		}{
			tripName.(string),
			username.(string),
		}, false), nil
	case "newFollower":
		username, ok := data["followerUsername"]
		if !ok {
			return nil, errors.New("missing username field")
		}
		return L.TAll(t, struct {
			Username string
		}{
			username.(string),
		}, false), nil
	case "newTripsExtracted":
		if fail, ok := data["isUpdateFailed"]; ok && fail.(bool) {
			if userFail, ok := data["isUserFailed"]; ok && userFail.(bool) {
				return L.TAll("newTripsTooManyAttemptsError", nil, false), nil
			} else {
				return L.TAll("newTripsError", nil, false), nil
			}

		}

		newTrips, ok := data["newIds"]
		if !ok {
			return nil, errors.New("missing newIds field")
		}

		updatedTrips, ok := data["updatedIds"]
		if !ok {
			return nil, errors.New("missing updatedIds field")
		}

		var countNewTrips, countUpdatedTrips int

		switch tmp := newTrips.(type) {
		case []interface{}:
			countNewTrips = len(tmp)
			break
		case []string:
			countNewTrips = len(tmp)
			break
		default:
			return nil, fmt.Errorf("Unsupported type: %T\n", tmp)
		}

		switch tmp := updatedTrips.(type) {
		case []interface{}:
			countUpdatedTrips = len(tmp)
			break
		case []string:
			countUpdatedTrips = len(tmp)
			break
		default:
			return nil, fmt.Errorf("Unsupported type: %T\n", tmp)
		}

		if countNewTrips == 0 && countUpdatedTrips == 0 {
			return L.TAll("newTripsEmpty", nil, false), nil
		}

		projection := struct {
			Created int
			Updated int
		}{
			countNewTrips,
			countUpdatedTrips,
		}

		if countNewTrips != 0 && countUpdatedTrips == 0 {
			return L.TAll("newTripsCreated", projection, false), nil
		}
		if countNewTrips == 0 && countUpdatedTrips != 0 {
			return L.TAll("newTripsUpdated", projection, false), nil
		}
		return L.TAll("newTripsCreatedAndUpdated", projection, false), nil
	default:
		return L.TAll(t, data, false), nil
	}
}

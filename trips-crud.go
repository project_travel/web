package server

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type CommentText struct {
	Text string `json:"comment"`
}

type Like struct {
	Username string `json:"username" bson:"username"`
	TripId   string `json:"tripId" bson:"tripId"`
	Created  *Time  `json:"created" bson:"created"`
}

type Comment struct {
	Id       bson.ObjectId `json:"id" bson:"_id"`
	Username string        `json:"username" bson:"username"`
	Created  *Time         `json:"created" bson:"created"`
	Modified *Time         `json:"modified,omitempty" bson:"modified,omitempty"`
	Text     string        `json:"text" bson:"text"`
}

type GetComments struct {
	Comments []Comment `bson:"comments"`
	Visible  bool      `bson:"visible"`
	Owners   []string  `bson:"_id"`
}

// Get list of users, whot liked trip
func (s *Server) getLikesTrip(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	search := bson.M{"tripId": id}

	likes := make([]*Like, 0)

	if err := s.DB.Collections["likes"].GetAll(search, nil, 0, 0, "_id", &likes); err != nil {
		return c.JSON(http.StatusBadRequest, ErrNoTrip.Localize(lang, s.translator))
	}

	tmp := make([]string, len(likes))
	for i, like := range likes {
		tmp[i] = like.Username
	}

	search = bson.M{"username": bson.M{"$in": tmp}}
	users := make([]*User, 0)
	if err := s.DB.Collections["users"].GetAll(search, bson.M{"password": 0}, 0, 0, "lastName,firstName,username", &users); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}
	return c.JSON(http.StatusOK, users)

}

func (s *Server) likeTrip(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	// Check user access to this trip

	if err := s.DB.Collections["trips"].Get(bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
	}, bson.M{"_id": 1}, nil); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	if err := s.DB.Collections["likes"].Create(bson.M{
		"username": username,
		"tripId":   id,
		"created":  Time{time.Now().UTC()},
	}); err != nil {
		return c.JSON(http.StatusBadRequest, ErrLikedTrip.Localize(lang, s.translator))
	}

	// Send push notifications
	go func() {
		trip := new(Trip)
		if err := s.DB.Collections["trips"].Get(bson.M{"_id": id}, bson.M{"owners": 1, "name": 1}, trip); err != nil {
			s.logger.Info("Can't get trip %s\n", id)
			return
		}
		for _, v := range trip.Owners {
			// don't send notifications to yourself,
			// if you liked your own trip
			if v == username {
				continue
			}
			go func(reciever string) {
				if err := s.Pusher.Send(reciever, "like", id.Hex(), trip.Name, username); err != nil {
					s.logger.WithError(err).Error("Can't send push")
				}
			}(v)
		}
	}()

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) dislikeTrip(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	if err := s.DB.Collections["trips"].Get(bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
	}, bson.M{"_id": 1}, nil); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	if err := s.DB.Collections["likes"].Delete(bson.M{
		"username": username,
		"tripId":   id,
	}); err != nil {
		return c.JSON(http.StatusBadRequest, ErrNotLikedTrip.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) updateTrip(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	trip := new(UpdateTrip)
	if err := c.Bind(trip); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	if trip.Name != nil {
		if *trip.Name == "" {
			return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "can't delete trip name"))
		}

		if !isValidTripName(*trip.Name) {
			return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "trip name must be shorter than 100 symbols"))
		}
	}

	trip.Modified = NewTime(time.Now())

	s.logger.Debug(*trip)

	search := bson.M{
		"_id":    id,
		"owners": username,
	}

	update := bson.M{
		"$set": trip,
	}

	if err := s.DB.Collections["trips"].Update(search, update); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) getComments(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	username := getUsernameFromToken(c)

	offset, size, _, err := setPagination(c, "-created")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search := bson.M{
		"_id":       id,
		"notShowTo": bson.M{"$ne": username},
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
	}

	projection := bson.M{
		"_id": 0,
		"comments": bson.M{
			"$slice": []int{offset, size},
		},
	}

	comments := struct {
		Comments []Comment
	}{make([]Comment, 0)}
	if err := s.DB.Collections["trips"].Get(search, projection, &comments); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusOK, comments.Comments)
}

func (s *Server) postComment(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	search := bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
	}

	text := new(CommentText)
	if err := c.Bind(text); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}

	if !isValidComment(text.Text) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "Comment can't be empty or be longer than 2000 symbols"))
	}

	comment := Comment{
		Id:       bson.NewObjectId(),
		Username: username,
		Text:     text.Text,
		Created:  NewTime(time.Now()),
	}

	update := bson.M{"$push": bson.M{"comments": comment}}
	if err := s.DB.Collections["trips"].Update(search, update); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	// Send push notifications
	go func() {
		trip := new(Trip)
		if err := s.DB.Collections["trips"].Get(bson.M{"_id": id}, bson.M{"owners": 1, "name": 1}, trip); err != nil {
			s.logger.Infof("Can't get trip %s\n", id)
			return
		}
		for _, v := range trip.Owners {
			// don't send notifications to yourself,
			// if you commented your own trip
			if v == username {
				continue
			}
			go func(reciever string) {
				if err := s.Pusher.Send(reciever, "comment", id.Hex(), trip.Name, username); err != nil {
					s.logger.WithError(err).Error("Can't send push")
				}
			}(v)
		}
	}()

	return c.JSON(http.StatusCreated, comment)
}

func (s *Server) updateComment(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	commentId, err := getId(c, "commentId")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	if err := s.DB.Collections["trips"].Get(bson.M{"_id": id}, bson.M{"_id": 1}, nil); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoTrip.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	text := new(CommentText)
	if err := c.Bind(text); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, ErrInvalidContent.Localize(lang, s.translator))
	}
	if !isValidComment(text.Text) {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, "Comment can't be empty or be longer than 2000 letters"))
	}

	search := bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
		"comments": bson.M{
			"$elemMatch": bson.M{"_id": commentId, "username": username},
		},
	}
	update := bson.M{"$set": bson.M{"comments.$.text": text.Text, "comments.$.modified": NewTime(time.Now())}}

	if err := s.DB.Collections["trips"].Update(search, update); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoComment.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) deleteComment(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)
	id, err := getId(c, "id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	commentId, err := getId(c, "commentId")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	if err := s.DB.Collections["trips"].Get(bson.M{"_id": id}, bson.M{"_id": 1}, nil); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoTrip.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	search := bson.M{
		"_id": id,
		"$or": []bson.M{
			bson.M{"owners": username},
			bson.M{"visible": true},
		},
		"comments": bson.M{
			"$elemMatch": bson.M{"_id": commentId, "username": username},
		},
	}
	update := bson.M{
		"$pull": bson.M{
			"comments": bson.M{
				"_id":      commentId,
				"username": username,
			},
		},
	}

	if err := s.DB.Collections["trips"].Update(search, update); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoComment.Localize(lang, s.translator))
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (s *Server) getTrip(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	id, err := getId(c, "id")
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}
	username := getUsernameFromToken(c)

	query := []bson.M{
		bson.M{
			"$match": bson.M{
				"_id":       id,
				"notShowTo": bson.M{"$ne": username},
				"$or": []bson.M{
					bson.M{"owners": username},
					bson.M{"visible": true},
				},
			},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "likes",
				"localField":   "_id",
				"foreignField": "tripId",
				"as":           "likes",
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":          1,
				"name":         1,
				"description":  1,
				"owners":       1,
				"start":        1,
				"end":          1,
				"countries":    1,
				"cities":       1,
				"countryCodes": 1,
				"hashtags":     1,
				"avatar":       1,
				"visible":      1,
				"modified":     1,
				"hasLiked": bson.M{
					"$cond": []interface{}{
						bson.M{
							"$eq": []interface{}{
								bson.M{
									"$size": bson.M{
										"$setIntersection": []interface{}{
											[]interface{}{username},
											"$likes.username",
										},
									},
								}, 1,
							},
						},
						true,
						false,
					},
				},
				"likes":    bson.M{"$size": "$likes"},
				"comments": bson.M{"$size": "$comments"},
			},
		},
	}

	trip := new(Trip)

	// Check user access to this trip
	if err := s.DB.Collections["trips"].Aggregate(query, trip); err != nil {
		return s.DB.handleNotFoundError(c, err, "trips", bson.M{"_id": id}, ErrNoTrip.Localize(lang, s.translator))
	}

	// Translate to provided language
	s.translator.Translate(trip, lang)

	return c.JSON(http.StatusOK, trip.Display(s.GetPath("api"), s.translator))

}

func (s *Server) getTrips(c echo.Context) error {
	username, _ := getUsername(c)
	currentUsername := getUsernameFromToken(c)

	lang := getLanguage(c, s.translator)

	offset, size, order, err := setPagination(c, "-start,-end,_id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	name := c.QueryParam("name")

	countries := strings.Split(c.QueryParam("countries"), ",")
	if countries[0] == "" {
		countries = countries[:0]
	} else {
		for i, v := range countries {
			// Convert countries to codes
			tmp, err := s.translator.T(v, lang, true)
			if err != nil {
				fmt.Println(err)
				continue
			}
			countries[i] = tmp
		}
	}

	cities := strings.Split(c.QueryParam("cities"), ",")
	if cities[0] == "" {
		cities = cities[:0]
	} else {
		for i, v := range cities {
			// Convert cities to codes
			tmp, err := s.translator.T(v, lang, true)
			if err != nil {
				fmt.Println(err)
				continue
			}
			// Convert codes to city names in english
			// tmp, err = s.translator.T(tmp, "en", false)
			// if err != nil {
			// 	fmt.Println(err)
			// 	continue
			// }
			cities[i] = tmp
		}
	}
	hashtags := strings.Split(c.QueryParam("hashtags"), ",")
	if hashtags[0] == "" {
		hashtags = hashtags[:0]
	}
	start := new(Time)
	if err := start.UnmarshalJSON([]byte(c.QueryParam("start"))); err != nil {
		s.logger.WithError(err).Info("Can't unmarshal start date")
		start = nil
	}
	end := new(Time)
	if err := end.UnmarshalJSON([]byte(c.QueryParam("end"))); err != nil {
		s.logger.WithError(err).Info("Can't unmarshal end date")
		end = nil
	}

	search := bson.M{
		"$and": []bson.M{
			bson.M{"notShowTo": bson.M{"$ne": currentUsername}},
			bson.M{"$or": []bson.M{
				bson.M{"owners": currentUsername},
				bson.M{"visible": true},
			},
			},
		},
	}

	if username != "" {
		search["$and"] = append(search["$and"].([]bson.M), bson.M{"owners": username})
	}

	and := []bson.M{}
	if name != "" {
		and = append(and, bson.M{"$text": bson.M{"$search": name}})
	}
	if len(countries) != 0 {
		and = append(and, bson.M{"countries": bson.M{"$elemMatch": bson.M{"$in": countries}}})
	}
	if len(cities) != 0 {
		and = append(and, bson.M{"cities": bson.M{"$elemMatch": bson.M{"id": bson.M{"$in": cities}}}})
	}
	if len(hashtags) != 0 {
		and = append(and, bson.M{"hashtags": bson.M{"$in": hashtags}})
	}
	if !start.Nil() {
		and = append(and, bson.M{"start": bson.M{"$gte": start}})

	}
	if !end.Nil() {
		and = append(and, bson.M{"end": bson.M{"$lte": end}})
	}
	if len(and) != 0 {
		search["$and"] = append(search["$and"].([]bson.M), bson.M{"$and": and})
	}

	trips := make([]*Trip, 0)

	if err := s.DB.Collections["trips"].AggregateAll([]bson.M{
		bson.M{
			"$match": search,
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "likes",
				"localField":   "_id",
				"foreignField": "tripId",
				"as":           "likes",
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":         1,
				"name":        1,
				"description": 1,
				"owners":      1,
				"start":       1,
				"end":         1,
				"countries":   1,
				"cities":      1,
				// "countryCodes": 1,
				"hashtags": 1,
				"avatar":   1,
				"visible":  1,
				"modified": 1,
				"hasLiked": hasLiked(currentUsername),
				"likes":    bson.M{"$size": "$likes"},
				"comments": bson.M{"$size": "$comments"},
			},
		},
		bson.M{
			"$sort": formSortQuery(order),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &trips); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Translate to provided language
	for i := 0; i < len(trips); i++ {
		s.translator.Translate(trips[i], lang)
		// Make them ready for sending to client
		trips[i].Display(s.GetPath("api"), s.translator)
	}

	return c.JSON(http.StatusOK, trips)
}

func (s *Server) getTripsFeed(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	offset, size, order, err := setPagination(c, "-start,-end,_id")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	user := new(User)
	if err := s.DB.Collections["users"].Get(bson.M{"username": username}, nil, user); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	trips := make([]*Trip, 0)

	if err := s.DB.Collections["trips"].AggregateAll([]bson.M{
		bson.M{
			"$match": bson.M{
				"owners": bson.M{"$in": user.Following},
				"$or": []bson.M{
					bson.M{"owners": username},
					bson.M{"visible": true},
				},
			},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "likes",
				"localField":   "_id",
				"foreignField": "tripId",
				"as":           "likes",
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":         1,
				"name":        1,
				"description": 1,
				"owners":      1,
				"start":       1,
				"end":         1,
				"countries":   1,
				"cities":      1,
				// "countryCodes": 1,
				"hashtags": 1,
				"avatar":   1,
				"visible":  1,
				"modified": 1,
				// "comments":     1, // show comment
				"hasLiked": hasLiked(username),
				"likes":    bson.M{"$size": "$likes"},
				"comments": bson.M{"$size": "$comments"},
			},
		},
		bson.M{
			"$sort": formSortQuery(order),
		},
		bson.M{
			"$skip": offset,
		},
		bson.M{
			"$limit": size,
		},
	}, &trips); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// Translate to provided language
	for i := 0; i < len(trips); i++ {
		s.translator.Translate(trips[i], lang)
		// Make them ready for sending to client
		trips[i].Display(s.GetPath("api"), s.translator)
	}

	return c.JSON(http.StatusOK, trips)
}

func (s *Server) getTripsRecommendations(c echo.Context) error {
	lang := getLanguage(c, s.translator)
	username := getUsernameFromToken(c)

	_, size, _, err := setPagination(c, "")
	if err != nil {
		return c.JSON(http.StatusBadRequest, ErrBadRequestWithError.Localize(lang, s.translator, err.Error()))
	}

	places := &struct {
		Body []*PlaceObject `json:recommendations`
	}{
		make([]*PlaceObject, 0),
	}
	if err := s.DB.Collections["users"].Get(
		bson.M{"username": username},
		bson.M{"recommendations": 1, "_id": 0},
		&places,
	); err != nil {
		if isNotFound(err) {
			return c.JSON(http.StatusBadRequest, ErrNoUser.Localize(lang, s.translator))
		} else {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}
	}

	placesIds := make([]string, len(places.Body))
	for i, place := range places.Body {
		placesIds[i] = place.Id
	}

	trips := make([]*Trip, 0)

	// Get history of requests
	requestedTrips := make([]bson.ObjectId, 0)
	if c.QueryParam("refresh") == "true" {
		s.removeTripsHistory(username)
	} else {
		requestedTrips = s.getTripsHistory(username)
	}

	if err := s.DB.Collections["trips"].AggregateAll([]bson.M{
		bson.M{
			"$match": bson.M{
				"_id":       bson.M{"$nin": requestedTrips},
				"countries": bson.M{"$elemMatch": bson.M{"id": bson.M{"$in": placesIds}}},
				"notShowTo": bson.M{"$ne": username},
				// not interested in users trips
				"visible": true,
				"owners": bson.M{
					"$ne": username,
				},
			},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "likes",
				"localField":   "_id",
				"foreignField": "tripId",
				"as":           "likes",
			},
		},
		bson.M{
			"$project": bson.M{
				"_id":         1,
				"name":        1,
				"description": 1,
				"owners":      1,
				"start":       1,
				"end":         1,
				"countries":   1,
				"cities":      1,
				// "countryCodes": 1,
				"hashtags": 1,
				"avatar":   1,
				"visible":  1,
				"modified": 1,
				// "comments":     1, // show comment
				"hasLiked": hasLiked(username),
				"likes":    bson.M{"$size": "$likes"},
				"comments": bson.M{"$size": "$comments"},
			},
		},
		bson.M{
			"$sample": bson.M{"size": size},
		},
	}, &trips); err != nil {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
	}

	// If nothing found give just something
	if len(trips) < size {
		additionalTrips := make([]*Trip, 0)
		if err := s.DB.Collections["trips"].AggregateAll([]bson.M{
			bson.M{
				"$match": bson.M{
					"_id":       bson.M{"$nin": requestedTrips},
					"visible":   true,
					"notShowTo": bson.M{"$ne": username},
					"owners": bson.M{
						"$ne": username,
					},
				},
			},
			bson.M{
				"$lookup": bson.M{
					"from":         "likes",
					"localField":   "_id",
					"foreignField": "tripId",
					"as":           "likes",
				},
			},
			bson.M{
				"$project": bson.M{
					"_id":         1,
					"name":        1,
					"description": 1,
					"owners":      1,
					"start":       1,
					"end":         1,
					"countries":   1,
					"cities":      1,
					// "countryCodes": 1,
					"hashtags": 1,
					"avatar":   1,
					"visible":  1,
					"modified": 1,
					// "comments":     1, // show comment
					"hasLiked": hasLiked(username),
					"likes":    bson.M{"$size": "$likes"},
					"comments": bson.M{"$size": "$comments"},
				},
			},
			bson.M{
				"$sample": bson.M{"size": size - len(trips)},
			},
		}, &additionalTrips); err != nil {
			return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, s.translator))
		}

		// Add additional trips
		for _, v := range additionalTrips {
			trips = append(trips, v)
		}
	}

	// Save trips to history
	s.saveTripsHistory(username, trips)

	// Translate to provided language
	for i := 0; i < len(trips); i++ {
		s.translator.Translate(trips[i], lang)
		// Make them ready for sending to client
		trips[i].Display(s.GetPath("api"), s.translator)
	}

	return c.JSON(http.StatusOK, trips)
}

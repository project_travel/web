package server

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/labstack/echo"

	"github.com/Sirupsen/logrus"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CallbackInterface interface {
	Do(bson.M) error
}

type DB struct {
	Server      *Server
	Session     *mgo.Session
	Collections map[string]*Collection
	Database    string // name of database in Mongo
	logger      *logrus.Entry
}

type Collection struct {
	Server *Server
	coll   *mgo.Collection
	logger *logrus.Entry
}

func (s *Server) InitDB(address string, logger *logrus.Logger) (*DB, error) {
	if address == "" {
		address = "mongodb://localhost/"
	}
	di, err := mgo.ParseURL(address) // parse connection string
	if err != nil {
		return nil, err
	}
	if di.Database == "" {
		di.Database = "testing"
	}
	session, err := mgo.DialWithInfo(di) // start connection
	if err != nil {
		return nil, err
	}

	db := &DB{
		Server:      s,
		Session:     session,
		Database:    di.Database,
		Collections: make(map[string]*Collection),
		logger: logger.WithFields(logrus.Fields{
			"db":      di.Database,
			"address": address,
		}),
	}
	if db.logger != nil {
		db.logger.Info("Database initiated!")
	}

	return db, nil
}

func (db *DB) AddCollection(name string, indexes []mgo.Index) error {
	coll := db.Session.DB(db.Database).C(name)
	for _, index := range indexes {
		if err := coll.EnsureIndex(index); err != nil {
			return errors.New(name + ": " + err.Error())
		}
	}

	db.Collections[name] = &Collection{
		Server: db.Server,
		coll:   coll,
		logger: db.logger.WithField("collection", name),
	}
	if db.Collections[name].logger != nil {
		db.Collections[name].logger.Debug("Collection to database is added")
	}

	return nil
}

func (this *Collection) Create(query interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	err := coll.Insert(query)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(query),
			"error": err,
		}).Debug("Create")
	}

	return err
}

func (this *Collection) GetAll(query, sel interface{}, skip, limit int, order string, res interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)

	if order == "" {
		order = "_id"
	}
	err := coll.Find(query).Select(sel).Sort(strings.Split(order, ",")...).Skip(skip).Limit(limit).All(res)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query":  loggerJSON(query),
			"select": loggerJSON(sel),
			"skip":   skip,
			"limit":  limit,
			"sort":   order,
			"error":  err,
		}).Debug("Get all")
	}
	return err

}

func (this *Collection) Aggregate(q interface{}, res interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)

	err := coll.Pipe(q).One(res)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(q),
			"error": err,
		}).Debug("Aggregate one")
	}

	return err
}

func (this *Collection) AggregateAll(q interface{}, res interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	err := coll.Pipe(q).All(res)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(q),
			"error": err,
		}).Debug("Aggregate all")
	}
	return err
}

func (this *Collection) Update(q1, q2 interface{}, options ...interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	var err error
	switch len(options) {
	case 0:
		err = coll.Update(q1, q2)
		break
	case 1:
		if v, ok := options[0].(bool); ok && v {
			_, err = coll.Upsert(q1, q2)
		} else {
			err = coll.Update(q1, q2)
		}
		break
	}

	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query":  loggerJSON(q1),
			"update": loggerJSON(q2),
			"error":  err,
		}).Debug("Update one")
	}

	return err
}

func (this *Collection) UpdateAll(q1, q2 interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	_, err := coll.UpdateAll(q1, q2)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query":  loggerJSON(q1),
			"update": loggerJSON(q2),
			"error":  err,
		}).Debug("Update all")
	}

	return err
}

func (this *Collection) FindAndModify(search, sel, update interface{}, isNew bool, res interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)

	_, err := coll.Find(search).Apply(mgo.Change{Update: update, ReturnNew: isNew}, res)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query":  loggerJSON(search),
			"update": loggerJSON(update),
			"select": loggerJSON(sel),
			"error":  err,
		}).Debug("Find and modify")
	}
	return err
}

func (this *Collection) DeleteAll(query interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	_, err := coll.RemoveAll(query)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(query),
			"error": err,
		}).Debug("Delete")
	}
	return err
}

func (this *Collection) Delete(query interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	err := coll.Remove(query)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(query),
			"error": err,
		}).Debug("Delete")
	}
	return err
}

func (this *Collection) Get(query interface{}, sel interface{}, res interface{}) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	err := coll.Find(query).Select(sel).One(res)
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query":  loggerJSON(query),
			"select": loggerJSON(sel),
			"error":  err,
		}).Debug("Get one")
	}

	return err
}

func (this *Collection) Count(query interface{}) (int, error) {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)
	n, err := coll.Find(query).Count()
	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(query),
			"error": err,
		}).Debug("Count")
	}
	return n, err
}

func (this *Collection) Iterate(query interface{}, sel interface{}, callback CallbackInterface) error {
	session := this.coll.Database.Session.Copy()
	defer session.Close()
	coll := session.DB(this.coll.Database.Name).C(this.coll.Name)

	iter := coll.Find(query).Select(sel).Iter()
	res := bson.M{}

	for iter.Next(&res) {
		if err := callback.Do(res); err != nil {
			if this.logger != nil {
				this.logger.WithFields(logrus.Fields{
					"query": loggerJSON(query),
					"sel":   loggerJSON(sel),
					"error": err,
				}).Error("Next iteration")
			}
			return err
		}

	}

	err := iter.Close()

	if this.logger != nil {
		this.logger.WithFields(logrus.Fields{
			"query": loggerJSON(query),
			"sel":   loggerJSON(sel),
			"error": err,
		}).Debug("Iterate")
	}

	return err
}

// handleNotFoundErrorDB tells when error occured because there's no such entry ot user just doesn't have access to it.
func (this *DB) handleNotFoundError(c echo.Context, err error, collection string, search bson.M, outErr *Error) error {
	lang := getLanguage(c, this.Server.translator)
	if isNotFound(err) {
		// Check that data exists. If so, tell that user can't have access to it.
		if err := this.Collections[collection].Get(search, nil, nil); err == nil {
			return c.JSON(http.StatusForbidden, ErrForbidden.Localize(lang, this.Server.translator))
		} else {
			return c.JSON(http.StatusBadRequest, outErr)
		}
	} else {
		return c.JSON(http.StatusInternalServerError, ErrInternal.Localize(lang, this.Server.translator))
	}
}

func loggerJSON(msg interface{}) interface{} {
	res, err := json.MarshalIndent(msg, "", "   ")
	if err != nil {
		return msg
	} else {
		return string(res)
	}
}

// IfInArrayProjection return query which tells if value in array field.
func IfInArrayProjection(value string, field string) bson.M {
	return bson.M{
		"$cond": []interface{}{
			bson.M{
				"$eq": []interface{}{
					bson.M{
						"$size": bson.M{
							"$setIntersection": []interface{}{
								[]interface{}{value},
								"$" + field,
							},
						},
					}, 1,
				},
			},
			true,
			false,
		},
		// "$cond": []interface{}{
		// 	bson.M{
		// 		"$eq": []interface{}{
		// 			"$" + field,
		// 			value,
		// 		},
		// 	},
		// 	true,
		// 	false,
		// },
	}
}

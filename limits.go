package server

import (
	"errors"
	"sync"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var TaskOverload error = errors.New("Too many tasks")

type TaskModule struct {
	Max    int `json:"max"`
	Time   int `json:"cooldown"`
	Server *Server
	sync.RWMutex
}

func (this *TaskModule) StartTask(username, source string) error {
	n, err := this.Server.DB.Collections["tasks"].Count(bson.M{
		"source":   source,
		"username": username,
	})
	if err != nil {
		return err
	}

	this.RLock()
	ok := n < this.Max
	this.RUnlock()

	if !ok {
		return TaskOverload
	}

	this.Lock()
	err = this.Server.DB.Collections["tasks"].Create(bson.M{
		"source":   source,
		"username": username,
		"created":  NewTime(time.Now())},
	)
	this.Unlock()
	return err
}

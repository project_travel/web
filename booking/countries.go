package booking

import (
	"math/rand"
	"sort"
	"strconv"
	"strings"
)

type Country struct {
	// Capital *City
	bigCities []*City
	cities    map[string]*City
}

type City struct {
	Name           string `json:"name"`
	CountryCode    string
	Code           string
	Link           string `json:"url"`
	NumberOfHotels int
}

type ByHotelsNumberDesc []*City

func (b ByHotelsNumberDesc) Len() int           { return len(b) }
func (b ByHotelsNumberDesc) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b ByHotelsNumberDesc) Less(i, j int) bool { return b[i].NumberOfHotels > b[j].NumberOfHotels }

func (this *Country) BigCity() *City {
	n := rand.Intn(len(this.bigCities))
	return this.bigCities[n]
}

func (this *Country) getBigCities() {
	cities := make([]*City, len(this.cities))
	i := 0
	for _, city := range this.cities {
		cities[i] = city
		i++
	}
	sort.Sort(ByHotelsNumberDesc(cities))
	length := len(cities)
	if length > 5 {
		length = 5
	}
	this.bigCities = cities[:length]
	return
}

func parseCityLine(line string) *City {
	// Example: Gorodilovo	1	ru	-2911174	http://www.booking.com/city/ru/gorodilovo.html
	data := strings.Split(line, "\t")
	n, _ := strconv.Atoi(data[1])
	return &City{
		Name:           data[0],
		CountryCode:    strings.ToUpper(data[2]),
		Code:           data[3],
		Link:           data[4],
		NumberOfHotels: n,
	}
}

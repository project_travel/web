package booking

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"time"
)

type Ad struct {
	Aid       string `json:"aid"`
	source    string
	countries map[string]*Country
}

func Init(aid, filename string) (*Ad, error) {
	// Get random big city
	rand.Seed(time.Now().Unix())
	ad := &Ad{Aid: aid, countries: make(map[string]*Country), source: "booking"}
	if err := ad.parseFile(filename); err != nil {
		return nil, err
	}
	for key, _ := range ad.countries {
		ad.countries[key].getBigCities()
	}
	return ad, nil
}

type GetResponse struct {
	Source    string `json:"source"`
	City      string `json:"city"`
	CityId    string `json:"-"`
	Country   string `json:"country"`
	CountryId string `json:"-"`
	Origin    bool   `json:"origin"`
	Link      string `json:"url"`
}

func (this GetResponse) GetCity() string {
	return this.City
}

func (this GetResponse) GetCountry() string {
	return this.Country
}

func (this GetResponse) GetCityId() string {
	return this.CityId
}

func (this GetResponse) GetCountryId() string {
	return this.CountryId
}

func (this *GetResponse) SetCountry(country string) {
	this.Country = country
}

func (this *GetResponse) SetCountryId(id string) {
	this.CountryId = id
}

func (this *GetResponse) SetCity(city string) {
	this.City = city
}

func (this *GetResponse) SetCityId(id string) {
	this.CityId = id
}

type Geo interface {
	GetCity() string
	GetCityId() string
	GetCountry() string
	GetCountryId() string
}

// Get gets list of links to given cities.
// If nothing found, it returns link to city with most hotels.
func (this *Ad) Get(in interface{}) []*GetResponse {
	inCities := make([]string, 0)
	inCountries := make([]string, 0)
	// inCities []string, inCountries []string
GeoLoop:
	for _, v := range in.([]interface{}) {
		if geo, ok := v.(Geo); ok {
			inCities = append(inCities, geo.GetCity())
			for _, country := range inCountries {
				if country == geo.GetCountryId() {
					continue GeoLoop
				}
			}
			inCountries = append(inCountries, geo.GetCountryId())
		}
	}
	// fmt.Println(inCountries, inCities)
	response := make([]*GetResponse, 0)
	for _, inCountry := range inCountries {
		// Check that has this country in our dict, if not skip to the next one
		country, ok := this.countries[inCountry]
		if !ok {
			continue
		}
		found := false
	Loop:
		for _, inCity := range inCities {
			// Search for such city in our dict
			pool := make([]*City, 0)
			for _, city := range country.cities {
				if city.Name == inCity {
					// Add link to response

					pool = append(pool, city)
					// response = append(response, city.Link)
					found = true
					// break
				}
			}
			switch len(pool) {
			case 0:
				continue Loop
			case 1:
				break
			default:
				// Return city with most hotels
				sort.Sort(ByHotelsNumberDesc(pool))
			}
			response = append(response, &GetResponse{
				Source:    this.source,
				City:      pool[0].Name,
				CountryId: pool[0].CountryCode,
				Link:      pool[0].Link,
				Origin:    true,
			})
		}
		if !found {
			bigCity := country.BigCity()
			response = append(response, &GetResponse{
				Source:    this.source,
				City:      bigCity.Name,
				CountryId: bigCity.CountryCode,
				Link:      bigCity.Link,
				Origin:    false,
			})
		}
	}
	return response
}

func (this *Ad) parseFile(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	first := true
	for scanner.Scan() {
		// Skip first line
		if first {
			first = false
			continue
		}
		city := parseCityLine(scanner.Text())
		if _, ok := this.countries[city.CountryCode]; !ok {
			this.countries[city.CountryCode] = &Country{cities: make(map[string]*City)}
		}
		// Add aid to city link
		this.countries[city.CountryCode].cities[city.Code] = this.buildLink(city)

	}
	return nil
}

func (this *Ad) buildLink(city *City) *City {
	city.Link = fmt.Sprintf("http://www.booking.com/searchresults.html?city=%s&aid=%s",
		city.Code, this.Aid)
	return city
}

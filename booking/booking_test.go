package booking

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	v, err := Init("1165100", "./data.tsv")
	if err != nil {
		t.Fatal(err)
	}

	resp := v.Get([]string{"London"}, []string{"US"})
	fmt.Println(resp)

	resp = v.Get([]string{"London1"}, []string{"US"})
	fmt.Println(resp)
}
